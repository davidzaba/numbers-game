import React from 'react';
import { DangerZone } from 'expo';
import { inject } from 'mobx-react/native';
import { StyleSheet, View, Animated } from 'react-native';

import { Timing } from '../Animations/Timing'

// Lottie
const { Lottie } = DangerZone
const LOTTIE_SUCCESS = require('../assets/lottie/success.json')

/**
 *
 */
@inject('gameStore')
export default class SuccessAnim extends React.Component {

  constructor (props) {
    super(props)

    this.state = {
      opacity: new Animated.Value(0),
      progress: new Animated.Value(0)
    }
  }

  componentWillMount() {
    this.lottieSize = Math.min(this.props.screenWidth, this.props.screenHeight) / 1.5

    this.topPosOffset = (this.props.screenHeight - this.lottieSize) / 2
    this.leftPosOffset = (this.props.screenWidth - this.lottieSize) / 2
  }

  componentDidMount() {
    this.playSuccess()
  }

  render() {
    return (
      <Animated.View style={[styles.container, {
        top: this.topPosOffset,
        left: this.leftPosOffset,
        width: this.lottieSize,
        height: this.lottieSize,
        opacity: this.state.opacity
      }]}>
        <Lottie
          source={LOTTIE_SUCCESS}
          progress={this.state.progress}
          loop={false}
          style={{width: this.lottieSize, height: this.lottieSize}} />
      </Animated.View>
    );
  }

  playSuccess() {
    (new Timing(200)).start(this.state.opacity, 1, 100)(
      () => {
        (new Timing(600)).start(this.state.progress, 1, 0, true)();
        (new Timing(200)).start(this.state.opacity, 0, 1000)(
          () => this.props.gameStore.setSuccessAnimationVisiblity(false)
        )
      }
    )
  }
}

/**
 * Styles
 */
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  }
});
