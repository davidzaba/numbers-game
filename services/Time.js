/**
 *
 * @type {{get: (())}}
 */
export const Time = {
  get() {
    return new Date().getTime()
  }
}
