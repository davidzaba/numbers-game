/**
 *
 */
export class CircleUtils {

  isEqual = true
  radius = 0
  size = 0
  cx = 0
  cy = 0

  /**
   *
   * @param isEqual
   */
  constructor (isEqual = true) {
    this.isEqual = isEqual
  }

  /**
   *
   * @param w
   * @param h
   * @param offsetFromRim
   */
  build(w, h, offsetFromRim = 0) {
    this.size = Math.min(w, h)

    this.cx = (this.isEqual ? this.size : w) / 2
    this.cy = (this.isEqual ? this.size : h) / 2

    this.radius = (this.size / 2) - offsetFromRim;
  }

  /**
   *
   * @return {number}
   */
  getSize() {
    return this.size
  }

  /**
   *
   * @return {number}
   */
  getCX() {
    return this.cx
  }

  /**
   *
   * @return {number}
   */
  getCY() {
    return this.cy
  }

  /**
   *
   * @return {number}
   */
  getRadius() {
    return this.radius
  }
}
