/**
 *
 * @type {{make: ((min, max))}}
 */
export const RandomFromRange = {
  make(min, max, fixed = 5) {
    return Number((Math.random() * (max - min) + min).toFixed(fixed))
  }
}
