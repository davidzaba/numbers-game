import React from 'react';
import { Audio } from 'expo';
import { inject, observer } from 'mobx-react/native';
import { Feather, Ionicons } from '@expo/vector-icons';
import { moderateScale, ScaledSheet } from 'react-native-size-matters';
import { StyleSheet, Text, View, TouchableHighlight } from 'react-native';

import { Routes } from '../Nav'

import { AudioFiles } from '../Constants/AudioFiles'

/**
 *
 */
@inject('gameStore') @observer
export default class Panel extends React.Component {

  constructor (props) {
    super(props);

    this.state = { btnSound: new Audio.Sound()}
  }

  async componentWillMount() {
    try {
      await Audio.setIsEnabledAsync(true)
      await this.state.btnSound.loadAsync(AudioFiles.BTN_PRESS)
    } catch (e) {}
  }

  async componentWillUnmount() {
    if (this.state.btnSound) {
      await this.state.btnSound.unloadAsync()
      this.state.btnSound = null
    }
  }

  render() {
    return (
      <View style={styles.container}>
        {
          this.props.children ||
          <View style={styles.title}>
            <Text style={styles.txt_title}>N U M B E R S</Text>
            <Text style={styles.txt_subtitle}>THE GAME</Text>

            <View style={[styles.infoBar]} />
            <View style={[styles.infoBar, styles.infoBottomBar]} />
          </View>
        }

        <View style={styles.delimiter} />

        <TouchableHighlight
          style={styles.play}
          onPressIn={this.playBtn.bind(this)}
          onPress={this.props.play.bind(this)}>
          <View style={styles.playIconContainer}>
            <Feather size={moderateScale(30)} name={'play'} color={'white'} />
          </View>
        </TouchableHighlight>

        <View style={styles.delimiter} />

        <TouchableHighlight
          style={styles.stats}
          onPressIn={this.playBtn.bind(this)}
          onPress={this.goToStats.bind(this)}>
          <View style={styles.statsIconContainer}>
            <Ionicons size={moderateScale(25)} name={'ios-stats-outline'} color={'white'} />
          </View>
        </TouchableHighlight>
      </View>
    );
  }

  async playBtn() {
    try {
      await this.state.btnSound.setOnPlaybackStatusUpdate(null)
      await this.state.btnSound.setPositionAsync(0)
      await this.state.btnSound.setVolumeAsync(1)
      await this.state.btnSound.playAsync()
    } catch (e) {}
  }

  goToStats() {
    this.props.gameStore.setNav(Routes.stats)
  }
}

const styles = ScaledSheet.create({
  container: {
    width: "200@s",
    height: "200@vs",
    flexDirection: 'column',
  },

  infoBar: {
    position: 'absolute',
    height: "8@s",
    backgroundColor: '#fff',
  },

  infoBottomBar: {
    left: "20@s",
    right: "20@s",
    bottom: 0,
  },

  title: {
    flex: 2,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#39CCCC',
  },

  txt_title: {
    fontSize: "25@ms0.5",
    color: 'white',
  },

  txt_subtitle: {
    fontSize: "12@ms0.5",
    color: 'white',
  },

  play: {
    flex: 2,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'orange',
  },

  playIconContainer: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  stats: {
    flex: 1,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0074D9',
  },

  statsIconContainer: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  delimiter: {
    width: "200@s",
    height: 5,
  }
})
