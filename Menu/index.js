import React from 'react';
import { reaction } from 'mobx';
import { StyleSheet, View } from 'react-native';
import { inject, observer } from 'mobx-react/native';
import { Feather, Ionicons } from '@expo/vector-icons';

import { Routes } from '../Nav'

import Loader from '../Loader'
import Panel from './Panel'
import MenuBG from './BG'

/**
 *
 */
@inject('gameStore') @observer
export default class Index extends React.Component {

  constructor (props) {
    super(props);

    this.state = { loader: true }
  }

  componentWillUnmount() {
    // discard observable reaction
    this.navObserver()
  }

  render() {
    return (
      <View style={styles.container}>
        <MenuBG onImgLoaded={this.hideLoader.bind(this)} />

        {/* Info panel */}
        <Panel play={this.play.bind(this)} />

        {/* Loader */}
        {
          this.state.loader &&
          <Loader />
        }
      </View>
    );
  }

  hideLoader () {
    this.setState({ loader: false})
  }

  play() {
    this.props.navigation.navigate(Routes.game)
  }

  /**
   * observable reactions
   */
  navObserver = reaction(
    () => this.props.gameStore.nav,
    (nav) => {
      this.props.navigation.navigate(nav.destination)
    }
  )
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
