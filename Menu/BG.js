import React from 'react';
import { StyleSheet, Text, View, Image, Animated } from 'react-native';

import { Timing } from '../Animations/Timing'

/**
 *
 */
export default class BG extends React.Component {

  constructor (props) {
    super(props);

    this.DURATION_SPIN = 500
    this.NUM_SIZE = 200

    this.state = { width: 0, height: 0, size: 0, imgLoaded: false }

    // init val for spinning animation
    this.anim = { spin: new Animated.Value(0) }

    // spinning animation for numbers in the background
    this.spinValue = this.anim.spin.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    })
  }

  render() {
    return (
      <View style={styles.container} onLayout={this.onLayout.bind(this)}>
        <Animated.View style={{
          width: this.state.size,
          height: this.state.size,
          transform: [{rotate: this.spinValue}]}}>
          <Image
            style={{
              position: 'absolute',
              width: this.state.size,
              height: this.state.size,
              resizeMode: 'contain'
            }}

            source={require('../assets/bg_numbers.png')}

            onLoad={this.onImgLoaded.bind(this)}
          />
        </Animated.View>
      </View>
    );
  }

  onLayout(e) {
    let { width, height } = e.nativeEvent.layout;

    let size = Math.min(width + 100, height + 100)

    this.setState({ width, height, size })
  }

  animSpin(endVal) {
    (new Timing(this.DURATION_SPIN)).start(this.anim.spin, endVal, 0, true)()
  }

  onImgLoaded() {
    this.props.onImgLoaded()
    this.animSpin(1)
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
