import React from 'react';
import Timeline from 'react-native-timeline-listview';
import { StyleSheet, Text, View } from 'react-native';
import { moderateScale, ScaledSheet } from 'react-native-size-matters';

import { Storage } from './Storage'

import Loader from './Loader'
import DummyHeader from './Header/dummy'

/**
 *
 */
export default class Stats extends React.Component {

  constructor (props) {
    super(props);

    this.state = {loaded: false, stats: []}
    this.loadStats()
  }

  render() {
    let statsTitle = <Text style={styles.top}>TOP SCORES</Text>

    // no stats available
    if (!this.state.stats.length)
      return (
        <View style={styles.container}>
          <DummyHeader />

          {statsTitle}

          <View>
            <Text style={styles.noScore}>No scores so far..</Text>
          </View>

          {/* Loader */}
          {
            !this.state.loaded &&
            <View style={styles.loaderContainer}>
              <Loader />
            </View>
          }
        </View>
      );

    // stats available
    return (
      <View style={styles.container}>
        <DummyHeader />

        {statsTitle}

        <Timeline
          data={this.state.stats}
          showTime={false}
          innerCircle={'dot'}
          lineColor='#3D9970'
          titleStyle={{fontSize: moderateScale(25), fontFamily: 'EncodeSansCondensed-Regular'}}
          descriptionStyle={{fontSize: moderateScale(13), color: 'gray'}} />
      </View>
    );
  }

  loadStats() {
    Storage.retrieve().then((stats) => {
      stats = stats && JSON.parse(stats) || []

      this.setState({ loaded: true, stats })
    })
  }
}

const styles = ScaledSheet.create({
  loaderContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  container: {
    flex: 1,
    padding: 15,
    backgroundColor: 'white',
  },

  noScore: {
    fontSize: "20@s",
    color: 'grey',
    fontFamily: 'EncodeSansCondensed-Regular',
  },

  top: {
    fontSize: "45@s",
    marginTop: 5,
    marginBottom: 25,
    fontFamily: 'EncodeSansCondensed-Regular',
  },
});
