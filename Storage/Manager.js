import { Choices } from '../Constants/Choices'
import { Base } from './Base'

/**
 * Manager
 */
export class StorageManager extends Base {
  constructor (range, score, stars, touches) {
    super(range, score, stars, touches)

    this.totalItems = 0
    this.rangeItems = 0

    this.statsItemSize = 0
    this.statsItemColor = ''
  }

  init () {
    let range = this.getCurrentRange()

    this.statsItemSize = range.statsCircleSize
    this.statsItemColor = range.color
  }

  setTotalItems (total) {
    this.totalItems = total
  }

  setRangeItems (total) {
    this.rangeItems = total
  }

  sort (stats) {
    let statsToSave = []
    stats = stats && JSON.parse(stats) || []

    stats.push({
      score: this.score,
      range: this.range,
      title: this.title,
      description: this.description,
      circleSize: this.statsItemSize,
      circleColor: this.statsItemColor,
    })

    Choices.map((choice) => {
      let range = choice.top

      let filteredRange = stats.filter((item) => Number(item.range) === Number(range))

      // sort results from best to worst
      let sortedStats = filteredRange.sort((a, b) => Number(a.score) < Number(b.score) ? 1 : -1)
      // keep top items from each range
      sortedStats = sortedStats.splice(0, this.rangeItems)

      statsToSave = statsToSave.concat(sortedStats)
    })

    let sortedStats = statsToSave.sort((a, b) => Number(a.score) < Number(b.score) ? 1 : -1)

    // keep top this.totalItems from each range
    return sortedStats.splice(0, this.totalItems)
  }

  getCurrentRange () {
    return Choices.filter((o) => Number(o.top) === Number(this.range))[0]
  }
}
