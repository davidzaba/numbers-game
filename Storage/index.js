import { SecureStore } from 'expo'

const STORE_KEY = 'cleverus.numbers.stats'

/**
 *
 * @type {object}
 */
export const Storage = {
  async save(data, key = STORE_KEY) {
    try {
      await SecureStore.setItemAsync(key, data)
    } catch (err) {}
  },

  async retrieve(key = STORE_KEY) {
    try {
      return await SecureStore.getItemAsync(key)
    } catch (err) {
      return ''
    }
  }
}
