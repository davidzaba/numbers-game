/**
 * Base
 */
export class Base {
  constructor (range, score, stars, touches) {
    this.stats = []

    this.range = range
    this.score = score
    this.stars = stars
    this.touches = touches
  }

  get title () {
    return `SCORE: ${this.score}`
  }

  get description () {
    return `Range: ${this.range} Stars: ${this.stars} Touches: ${this.touches}`
  }
}
