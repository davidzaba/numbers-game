import React from 'react';
import { AppLoading, Font, SplashScreen } from 'expo'

import { FontSeries } from './Constants/Font'

import Nav from './Nav'

/**
 * App
 */
export default class App extends React.Component {

  constructor (props) {
    super(props)

    this.state = { isReady: false }
  }

  componentDidMount() {
    SplashScreen.preventAutoHide()
    this.loadResources()
  }

  render() {
    if (!this.state.isReady) {
      return null
    }

    return <Nav />
  }

  async loadResources() {
    await Font.loadAsync(FontSeries.condensed)
    this.setState({ isReady: true})
    SplashScreen.hide()
  }
}
