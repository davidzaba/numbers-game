import React from 'react';
import { reaction } from 'mobx';
import { Svg, DangerZone } from 'expo';
import { inject, observer } from 'mobx-react/native';
import { StyleSheet, View, Text, Animated, TouchableWithoutFeedback } from 'react-native';

import { RandomFromRange } from '../services/Randomizer'
import { CircleUtils } from '../services/Circle'

import { Timing } from '../Animations/Timing'

// SVG
const { Circle } = Svg
const OuterCircle = Animated.createAnimatedComponent(Circle)
const InnerCircle = Animated.createAnimatedComponent(Circle)

// Lottie
const { Lottie } = DangerZone
const LOTTIE_TIMER = require('../assets/lottie/timer.json')

/**
 *
 */
@inject('gameStore', 'choicesStore', 'prohibitionStore') @observer
export default class Number extends React.Component {

  constructor (props) {
    super(props)

    this.state = {
      rInner: 0,
      rOuter: 0,
      layout: false,
      animation: null,
      isPunished: false,
      progress: new Animated.Value(0),
      numOpacity: new Animated.Value(1),
      localRandNum: this.generateRandNumber(),
    }

    this.isSelected = false
    this.isResolving = false
    this.isUnmounted = false

    this.width = this.height = 0
    this.radius = this.cx = this.cy = this.size = this.lottieSize = 0

    // references
    this.outerCircle = null;
    this.innerCircle = null;
  }

  componentWillUnmount() {
    this.isUnmounted = true

    // discard observable reaction
    this.selectionObserver()
  }

  render() {
    return (
      <View style={styles.cell} onLayout={this.onLayout.bind(this)}>
        {this.renderChildren()}
      </View>
    );
  }

  renderChildren() {
    return (
      <React.Fragment>
        {/* Circular wrapper for the numeric choice below */}
        {
          this.state.layout &&
          <Svg width={this.size} height={this.size} style={styles.svgContainer}>
            <OuterCircle
              ref={(component) => this.outerCircle = component}
              cx={this.cx}
              cy={this.cy}
              r={this.state.rOuter}
              fill='transparent' />

            <InnerCircle
              ref={(component) => this.innerCircle = component}
              cx={this.cx}
              cy={this.cy}
              r={this.state.rInner}
              fill='transparent'
              stroke='black'
              strokeWidth='2' />
          </Svg>
        }

        {/* Circular punishment progress */}
        {/* Nest Lottie in view with the same size to properly center animated content */}
        {
          this.state.isPunished &&
          <View style={[styles.penaltyContainer, {width: this.lottieSize, height: this.lottieSize}]}>
            <Lottie
              ref={(animation) => this.animation = animation}
              source={LOTTIE_TIMER}
              progress={this.state.progress}
              loop={false}
              style={{width: this.lottieSize, height: this.lottieSize}} />
          </View>
        }

        {/* Numeric choice */}
        <TouchableWithoutFeedback onPress={this.toggleFocus.bind(this)}>
          <Animated.View
            style={[styles.numBox, {opacity: this.state.numOpacity, height: this.size}]}>
            <Text style={styles.text}>
              {this.state.localRandNum}
            </Text>
          </Animated.View>
        </TouchableWithoutFeedback>
      </React.Fragment>
    );
  }

  onLayout(e) {
    if (this.cx && this.cy) {
      return
    }

    let { width, height } = e.nativeEvent.layout

    this.makeGrid(width, height)
  }

  makeGrid(width, height) {
    let utils = new CircleUtils(false)
    utils.build(width, height, 7)

    this.size = utils.getSize()
    this.lottieSize = this.size + 15

    this.cx = utils.getCX()
    this.cy = utils.getCY()

    this.radius = utils.getRadius();

    let outerRadius = this.radius
    let innerRadius = this.radius - 5

    // mark layout as ready
    !this.isUnmounted && this.setState({
      layout: true,
      rOuter: outerRadius,
      rInner: innerRadius,
    });

    // animated props
    this.innerCircleStrokeAnim = { strokeWidth: new Animated.Value(2) }
    this.innerCircleAnim = { r: new Animated.Value(innerRadius) }

    // regularly update props to ensure smooth animation
    this.innerCircleStrokeAnim.strokeWidth.addListener((width) => {
      this.innerCircle && this.innerCircle.setNativeProps({ strokeWidth: width.value.toString()})
    })

    // regularly update props to ensure smooth animation
    this.innerCircleAnim.r.addListener((radius) => {
      this.innerCircle && this.innerCircle.setNativeProps({ r: radius.value.toString()})
    })

    // label choice as loaded/refreshed
    this.props.choicesStore.confirmLoadedChoice()
    this.props.choicesStore.confirmRefreshedChoice()
  }

  toggleFocus() {
    if (this.isResolving) {
      return
    }

    if (!this.isSelected && this.props.gameStore.isInVerifiableState) {
      this.props.prohibitionStore.enableProhibitedState()
      return
    }

    this.isSelected = !this.isSelected

    if (this.isSelected) {
      this.getFocusInAnim().start()
      this.props.gameStore.addSelection({ num: this.state.localRandNum, id: this.props.id })
    } else {
      this.getFocusOutAnim().start()
      this.props.gameStore.removeSelection({ num: this.state.localRandNum, id: this.props.id })
    }
  }

  excludeFromGame({ punish = false } = {}) {
    this.isResolving = true
    this.props.gameStore.resolveSelection(true)

    // animate
    let sequence = Animated.sequence([
      (new Timing(500, false))
        .start(this.state.numOpacity, 0, 0, true),
      Animated.parallel([
        (new Timing(300, false))
          .start(this.innerCircleStrokeAnim.strokeWidth, 1, 400, true),
        (new Timing(300, false))
          .start(this.innerCircleAnim.r, this.radius - 15, 800, true),
      ])
    ])

    // reward or punish -> inflate or deflate circular countdown
    this.props.gameStore.jump(punish)

    if (punish) {
      sequence.start(
        () => {
          let sequence = Animated.sequence([
            (new Timing(4000, false)).start(this.state.progress, 1, 0, true),
            (new Timing(1000, false)).start(this.state.progress, 0, 0, true),
          ])

          sequence.start(() => this.includeInGame())
        }
      )

      !this.isUnmounted && this.setState({ isPunished: true});
    } else {
      sequence.start(
        () => {
          this.includeInGame()
        }
      )
    }
  }

  includeInGame() {
    !this.isUnmounted && this.setState({ localRandNum: this.generateRandNumber() })

    this.getFocusOutAnim().start(
      () => {
        this.isResolving = false;
        this.props.gameStore.resolveSelection(false);

        !this.isUnmounted && this.setState({ isPunished: false })
      }
    )
  }

  refreshGame() {
    this.isResolving = true
    this.props.gameStore.resolveSelection(true)

    this.includeInGame()
  }

  getFocusInAnim() {
    return Animated.parallel([
      (new Timing(250, false))
        .start(this.innerCircleStrokeAnim.strokeWidth, 5, 0, true),
      (new Timing(250, false))
        .start(this.innerCircleAnim.r, this.radius - 2, 0, true),
    ])
  }

  getFocusOutAnim() {
    return Animated.sequence([
      Animated.parallel([
        (new Timing(250, false))
          .start(this.innerCircleStrokeAnim.strokeWidth, 2, 0, true),
        (new Timing(250, false))
          .start(this.innerCircleAnim.r, this.radius - 5, 0, true)
      ]),
      (new Timing(500, false))
        .start(this.state.numOpacity, 1, 500, true)
    ])
  }

  /**
   *
   * @return {*}
   */
  generateRandNumber() {
    return RandomFromRange.make(0, this.props.gameStore.selectedTop, 0)
  }

  /**
   * observable reactions
   */
  selectionObserver = reaction(
    () => this.props.gameStore.tip,
    (tip) => {
      // check for match inside selections
      this.props.gameStore.selections.forEach((o) => {
        o.id === this.props.id && (this.isSelected = true)
      })

      this.isSelected && (() => {
        tip.isMatch ?
          this.excludeFromGame() :
          this.excludeFromGame({ punish: true })

        this.props.gameStore.resetSelections()
        this.isSelected = false
      })()
    }
  )
}

/**
 * Styles
 */
const styles = StyleSheet.create({
  cell: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },

  svgContainer: {
    flex: 1,
    position: 'absolute',
    top: 0,
    left: 0,
  },

  penaltyContainer: {
    position: 'absolute'
  },

  numBox: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  text: {
    fontSize: 30,
    textAlign: 'center',
    fontFamily: 'EncodeSansCondensed-Regular'
  }
})
