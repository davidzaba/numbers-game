import React from 'react';
import { reaction } from 'mobx';
import { inject, observer } from 'mobx-react/native';
import { StyleSheet, View, Animated } from 'react-native';
import { moderateScale } from 'react-native-size-matters';

import { Grid } from '../Constants/Grid'

import { Timing } from '../Animations/Timing'

import Number from './Number'

/**
 *
 */
@inject('gameStore', 'choicesStore', 'layoutStore') @observer
export default class Numbers extends React.Component {

  constructor (props) {
    super(props)

    this.state = { gridReady: false, viewOpacity: new Animated.Value(0) }

    this.isVisible = true

    this.cols = []
    this.rows = []
  }

  componentWillUnmount() {
    this.viewObserver()
    this.readyChoicesObserver()
    this.refreshedChoicesObserver()
  }

  render() {
    return (
      <Animated.View
        style={[styles.container, {opacity: this.state.viewOpacity}]}
        onLayout={this.onLayout.bind(this)}>
        {
          this.props.layoutStore.numericLayoutReady &&
          this.state.gridReady &&
          this.rows.map((row) => {
            let rowId = `${this.props.groupId}-${row + 1}`

            return (
              <View key={rowId} style={styles.row}>
                {
                  this.cols.map((col) => {
                    let colId = `${this.props.groupId}-${(row + 1)}-${col}`

                    return <Number key={colId} id={colId} />
                  })
                }
              </View>
            )
          })
        }
      </Animated.View>
    );
  }

  onLayout(e) {
    let { width, height } = e.nativeEvent.layout

    let rows = 0, cols = 0

    for (let resolution of Grid.breakpoints.width) {
      if (width < resolution) {
        cols = this.getGridNum(resolution)
        break
      }
    }

    for (let resolution of Grid.breakpoints.height) {
      if (height < resolution) {
        rows = this.getGridNum(resolution)
        break
      }
    }

    for (let i = 0; i < rows; i++) {
      this.rows.push(i)
    }

    for (let i = 0; i < cols; i++) {
      this.cols.push(i)
    }

    // all calculations for grid have been made
    this.setState({ gridReady: true })

    // prep all available choices
    this.props.choicesStore.setAvailableChoices(cols * rows)
  }

  getGridNum(resolution) {
    return Math.ceil(resolution / moderateScale(Grid.cellSize))
  }

  /**
   * observable reactions
   */
  viewObserver = reaction(
    () => this.props.gameStore.isPaused,
    (isPaused) => {
      (new Timing(200)).start(this.state.viewOpacity, isPaused ? 0 : 1, 0, true)()
    }
  )

  readyChoicesObserver = reaction(
    () => this.props.choicesStore.loadedChoices,
    () => {
      this.props.choicesStore.choicesAreReady && (() => {
        (new Timing(400)).start(this.state.viewOpacity, 1, this.props.delay, true)()
      })()
    }
  )

  refreshedChoicesObserver = reaction(
    () => this.props.choicesStore.refreshedChoices,
    (choices) => {
      !choices && (() => {
        (new Timing(400)).start(this.state.viewOpacity, 0, this.props.delay, true)(
          () => {
            this.setState({ gridReady: false });
            this.setState({ gridReady: true });
          }
        )
      })();

      this.props.choicesStore.choicesAreRefreshed && (() => {
        (new Timing(400)).start(this.state.viewOpacity, 1, this.props.delay, true)()
      })()
    }
  )
}

/**
 * Styles
 */
const styles = StyleSheet.create({
  container:{
    flex: 4,
    alignItems: 'center',
    justifyContent: 'center',
    opacity: .8,
  },

  row: {
    flex: 1,
    flexDirection: 'row'
  },

  successContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'red'
  }
});
