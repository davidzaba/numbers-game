import React from 'react';
import { StyleSheet, View } from 'react-native';

import { Config } from '../Constants/Config'

/**
 *
 */
export default class DummyHeader extends React.Component {

  constructor (props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container} />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: Config.HEADER_HEIGHT,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
});
