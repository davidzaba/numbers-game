import React from 'react';
import { StyleSheet, View } from 'react-native';

/**
 *
 */
export default class Mask extends React.Component {

  constructor (props) {
    super(props);
  }

  render() {
    return (
      <View style={[styles.container, {backgroundColor: this.props.bgColor}]} />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    opacity: .4,
  },
});
