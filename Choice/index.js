import React from 'react';
import { Audio } from 'expo';
import { reaction } from 'mobx';
import { EvilIcons } from '@expo/vector-icons';
import { inject, observer } from 'mobx-react/native';
import { moderateScale, ScaledSheet } from 'react-native-size-matters';
import { StyleSheet, Text, View, Animated, TouchableHighlight } from 'react-native';

import { AudioFiles } from '../Constants/AudioFiles'
import { Choices } from '../Constants/Choices'
import { Timing } from '../Animations/Timing'
import Tween from '../Tweener'

/**
 *
 */
@inject('gameStore', 'maskStore', 'layoutStore') @observer
export default class Choice extends React.Component {

  constructor (props) {
    super(props);

    this.state = { btnSound: new Audio.Sound(), iconBoxWidth: new Animated.Value(0) }
  }

  async componentWillMount() {
    try {
      await Audio.setIsEnabledAsync(true)
      await this.state.btnSound.loadAsync(AudioFiles.BTN_PRESS)
    } catch (e) {}

  }

  componentDidMount() {
    setTimeout(() => this.highlightChoiceStrip(), 400)
  }

  async componentWillUnmount() {
    // discard observable reaction
    this.gameOverObserver()
    this.tweenObserver()

    // discard sound assets
    if (this.state.btnSound) {
      await this.state.btnSound.unloadAsync()
      this.state.btnSound = null
    }
  }

  render() {
    return (
      <View style={styles.container}>
        {/* Mask */}
        <View style={styles.mask} />

        {/* Choice has been made -> tween the selected number */}
        {
          !!this.props.gameStore.selectedTop &&
          <Tween maxTween={this.props.gameStore.selectedTop} />
        }

        {/* Choice has not been made -> offer numbers to pick */}
        {
          !this.props.gameStore.selectedTop &&
          <View style={styles.btnContainer}>
            {
              Choices.map((choice) =>
                <TouchableHighlight
                  key={choice.top}
                  style={[styles.btn, {backgroundColor: choice.color}]}
                  onPressIn={this.playBtn.bind(this)}
                  onPress={this.makeChoice.bind(this, choice.top, choice.duration)}>

                  <View style={styles.choiceContainer}>
                    <Animated.View style={[styles.icon, {width: this.state.iconBoxWidth}]}>
                      <EvilIcons size={moderateScale(30)} name={'arrow-right'} color={'white'} />
                    </Animated.View>
                    <View style={styles.textContainer}>
                      <Text style={styles.text}>{String(choice.top)}</Text>
                    </View>
                  </View>
                </TouchableHighlight>
              )}
          </View>
        }
      </View>
    );
  }

  makeChoice(top, duration) {
    this.props.gameStore.setChoiceDuration(duration)
    this.props.gameStore.setChoiceTop(top)
    this.props.maskStore.activate()
  }

  highlightChoiceStrip() {
    (new Timing(200)).start(this.state.iconBoxWidth, moderateScale(40))(
      () => this.props.layoutStore.resetNumericLayout()
    )
  }

  async playBtn() {
    try {
      await this.state.btnSound.setOnPlaybackStatusUpdate(null)
      await this.state.btnSound.setPositionAsync(0)
      await this.state.btnSound.setVolumeAsync(1)
      await this.state.btnSound.playAsync()
    } catch (e) {}
  }

  /**
   * observable reactions
   */
  tweenObserver = reaction(
    () => this.props.gameStore.tweenReady,
    (tweenReady) => {
      tweenReady && this.props.layoutStore.makeNumericLayoutReady()
    }
  )

  gameOverObserver = reaction(
    () => this.props.gameStore.gameOver,
    (gameOver) => {
      // A new game is about to start -> highlight the choice-buttons.
      !gameOver && this.highlightChoiceStrip()
    }
  )
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },

  mask: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    opacity: .5,
    backgroundColor: '#AAAAAA',
  },

  btnContainer: {
    flex: 1,
    flexDirection: 'row',
  },

  btn: {
    flex: 1,
  },

  choiceContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },

  textContainer: {
    flex: 2,
    paddingBottom: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },

  text: {
    color: 'white',
    fontSize: "35@ms0.3",
    fontFamily: 'EncodeSansCondensed-Black',
  },

  icon: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
