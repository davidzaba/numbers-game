import React from 'react';
import { DangerZone } from 'expo';
import { StyleSheet, View } from 'react-native';
import { moderateScale } from 'react-native-size-matters';

// Lottie
const { Lottie } = DangerZone
const LOTTIE_PRELOADER = require('../assets/lottie/preloader.json')

/**
 *
 */
export default class Loader extends React.Component {

  constructor (props) {
    super(props);

    this.LOADER_SIZE = 300;
  }

  componentDidMount() {
    this.showLoader()
  }

  render() {
    return (
      <View style={styles.container}>
        <Lottie
          ref={(animation) => this.animation = animation}
          source={LOTTIE_PRELOADER}
          loop={true}
          style={{
            width: moderateScale(this.LOADER_SIZE),
            height: moderateScale(this.LOADER_SIZE)
          }} />
      </View>
    );
  }

  showLoader() {
    this.animation && this.animation.play();
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
});
