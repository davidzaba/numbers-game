import React from 'react';
import { reaction } from 'mobx';
import { DangerZone } from 'expo';
import { inject, observer } from 'mobx-react/native';
import { StyleSheet, View, Text, Animated } from 'react-native';
import { moderateScale, ScaledSheet } from 'react-native-size-matters';

import { Timing } from '../Animations/Timing'

// Lottie
const { Lottie } = DangerZone
const RATING_ANIM = require('../assets/lottie/star.json')

/**
 *
 */
@inject('gameStore') @observer
export default class RatingStars extends React.Component {

  constructor (props) {
    super(props)

    this.state = { width: 0, height: 0, progress: new Animated.Value(0) }

    this.starAnimation = null
    this.STAR_LIMIT = 5
    this.history = []

    this.STAR_MAX_PROGRESS = .8
    this.STAR_OUT_DURATION = 1000
    this.STAR_IN_DURATION = 1500
  }

  componentDidMount() {
    this.starAnimation &&
    (new Timing(this.STAR_IN_DURATION)).start(this.state.progress, this.STAR_MAX_PROGRESS, 500)()
  }

  componentWillUnmount() {
    this.selectionObserver()
  }

  render() {
    return (
      <View style={styles.ratingContainer} onLayout={this.onLayout.bind(this)}>
        <Lottie
          ref={(animation) => this.starAnimation = animation}
          source={RATING_ANIM}
          progress={this.state.progress}
          loop={false}
          style={[
            styles.ratingAnim,
            {
              width: moderateScale(this.state.width),
              height: moderateScale(this.state.height)
            }
          ]} />

        <Text style={styles.starsCount}>x {this.props.gameStore.stars}</Text>
      </View>
    );
  }

  onLayout(e) {
    let { width, height } = e.nativeEvent.layout

    this.setState({ width, height })
  }

  setRating(matches) {
    let starsCount = Math.floor(Math.abs((matches / this.STAR_LIMIT)))

    starsCount !== this.history[this.history.length - 1] &&
    (new Timing(this.STAR_OUT_DURATION)).start(this.state.progress, 0)(
      () => (new Timing(this.STAR_IN_DURATION)).start(this.state.progress, this.STAR_MAX_PROGRESS)()
    )

    this.history.push(starsCount)
    this.props.gameStore.setStarsCount(starsCount)
  }

  /**
   * observable reactions
   */
  selectionObserver = reaction(
    () => this.props.gameStore.tip,
    (tip) => this.setRating(tip.matches)
  )
}

/**
 * Styles
 */
const styles = ScaledSheet.create({
  ratingContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },

  ratingAnim: {
    position: 'absolute',
  },

  starsCount: {
    flex: 1,
    fontSize: "10@s",
    textAlign: 'right',
    paddingRight: 10,
  }
});
