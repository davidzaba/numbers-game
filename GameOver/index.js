import React from 'react';
import { DangerZone } from 'expo';
import { inject, observer } from 'mobx-react/native';
import { StyleSheet, Text, View, Animated } from 'react-native';
import { moderateScale, ScaledSheet } from 'react-native-size-matters';

import { Timing } from '../Animations/Timing'
import Panel from '../Menu/Panel'

// Lottie
const { Lottie } = DangerZone
const CONFETTI_ANIM = require('../assets/lottie/confetti.json')

/**
 *
 */
@inject('gameStore') @observer
export default class GameOver extends React.Component {

  constructor (props) {
    super(props);

    this.OPACITY_DURATION = 300

    this.state = {
      progress: new Animated.Value(0),
      containerOpacity: new Animated.Value(0),
    }

    this.hasScore = false;
  }

  componentDidMount() {
    (new Timing(this.OPACITY_DURATION))
      .start(this.state.containerOpacity, 1, 0, true)();

    this.hasScore = this.props.gameStore.score
    this.hasScore && this.anim()
  }

  render() {
    // game over animation only when score > 0
    let gameOverAnim;

    if (this.hasScore) {
      gameOverAnim = <View style={styles.bgAnimContainer}>
        <Lottie
          source={CONFETTI_ANIM}
          progress={this.state.progress}
          loop={false}
          style={styles.bgAnim} />
      </View>
    } else {
      gameOverAnim = null
    }

    // out
    return (
      <Animated.View style={[styles.container, {opacity: this.state.containerOpacity}]} onLayout={this.onLayout.bind(this)}>
        <View style={styles.bg} />

        {/* BG Animation (Success / Failure) */}
        {gameOverAnim}

        {/* Info panel */}
        <View style={styles.infoContainer}>
          <Panel play={this.play.bind(this)}>
            <View style={styles.gameOver}>
              <Text style={styles.txt_gameOver}>Game Over</Text>
              <Text style={styles.txt_score}>Score:<Text style={styles.txt_scoreNum}> {this.props.gameStore.score}</Text></Text>
            </View>
          </Panel>
        </View>
      </Animated.View>
    );
  }

  onLayout(e) {
    let { width, height } = e.nativeEvent.layout

    this.setState({ width, height })
  }

  play() {
    this.props.playAgain()
  }

  anim() {
    (function _anim(ctx) {
      (new Timing(1500)).start(ctx.state.progress, 1, 500, true)(
        () => {
          ctx.setState({ progress: new Animated.Value(0)})
          _anim(ctx)}
      );
    })(this)
  }
}

const styles = ScaledSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },

  bg: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    opacity: .5,
  },

  infoContainer: {
    width: "200@s",
    height: "200@s",
    flexDirection: 'column',
  },

  gameOver: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },

  txt_gameOver: {
    fontSize: "35@s",
    fontFamily: 'EncodeSansCondensed-Regular'
  },

  txt_score: {
    fontSize: "17@s",
    fontFamily: 'EncodeSansCondensed-Regular'
  },

  txt_scoreNum: {
    fontFamily: 'EncodeSansCondensed-Black'
  },

  bgAnimContainer: {
    position: 'absolute',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  bgAnim: {
    width: '300@s',
    height: '300@s',
  },
});
