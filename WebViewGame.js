import React from 'react';
import { StyleSheet, Text, View, Button, WebView } from 'react-native';

import Header from './components/Header'

export default class WebViewGame extends React.Component {

  constructor (props) {
    super(props);

    this.state = {
      url: 'https://www.seznam.cz',
    }
  }

  componentDidMount() {

  }

  componentWillUnmount() {

  }

  render() {
    return (
      <View style={styles.container}>
        <Header title={"URL: " + this.state.url} />

        <Button title="PROD" onPress={() => this.setState({ url: 'https://www.mall.cz/' })} />
        <Button title="TEST" onPress={() => this.setState({ url: 'https://www.mall.cz.test/' })} />

        <View style={styles.wv}>
          <WebView
            source={{ uri: 'https://www.mall.cz.test/' }} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  wv: {
    flex: 10
  }
});
