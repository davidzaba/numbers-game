import React from 'react';
import axios from 'axios';
import { StyleSheet, View, Button } from 'react-native';
import { Ionicons, FontAwesome, Entypo } from '@expo/vector-icons';

import Header from './components/Header'
import Score from './components/Score'
import Card from './components/Card'

import './helpers'

export default class MemoryGame extends React.Component {

  constructor (props) {
    super(props);

    let sources = {
      FontAwesome,
      Entypo,
      Ionicons,
    }

    let cards = [
      {
        src: 'FontAwesome',
        name: 'heart',
        color: 'red'
      },
      {
        src: 'Entypo',
        name: 'feather',
        color: '#7d4b12'
      },
      {
        src: 'Entypo',
        name: 'flashlight',
        color: '#f7911f'
      },
      {
        src: 'Entypo',
        name: 'flower',
        color: '#37b24d'
      },
      {
        src: 'Entypo',
        name: 'moon',
        color: '#ffd43b'
      },
      {
        src: 'Entypo',
        name: 'youtube',
        color: '#FF0000'
      },
      {
        src: 'Entypo',
        name: 'shop',
        color: '#5f5f5f'
      },
      {
        src: 'FontAwesome',
        name: 'github',
        color: '#24292e'
      },
      {
        src: 'FontAwesome',
        name: 'skype',
        color: '#1686D9'
      },
      {
        src: 'FontAwesome',
        name: 'send',
        color: '#1c7cd6'
      },
      {
        src: 'Ionicons',
        name: 'ios-magnet',
        color: '#d61c1c'
      },
      {
        src: 'Ionicons',
        name: 'logo-facebook',
        color: '#3C5B9B'
      }
    ];

    let clone = JSON.parse(JSON.stringify(cards));
    this.cards = cards.concat(clone);

    // add the ID, source and set default state for each card
    this.cards.map(card => {
      let id = Math.random().toString(36).substring(7);

      card.id = id;
      card.src = sources[card.src];
      card.isOpen = false;
    });

    this.cards = this.cards.shuffle();

    this.state = {
      currentSelection: [],
      selectedPairs: [],
      cards: this.cards,
      score: 0,
    }

    axios.get("https://www.mall.cz/api/searching/suggest?q=iphone&timestamp=1526846056641" )
      .then(data => {
        console.log("FETCH -PROD- DATA", 1111111111)
      })
      .catch(e => {
        console.log("FETCH -PROD- ERROR", e)
      });

    axios.get( "https://www.mall.cz.test/api/searching/suggest?q=iphone&timestamp=1526846056641" )
      .then(data => {
        console.log("FETCH -TEST- DATA", 1111111111)
      })
      .catch(e => {
        console.log("FETCH -PROD- ERROR", e)
      });
  }

  componentWillMount() {
  }

  render() {
   return (
      <View style={styles.container}>
        <Button title="To AccelerGame" onPress={() => this.props.navigation.navigate('SensGame')} />
        <Button title="To WebView" onPress={() => this.props.navigation.navigate('WebGame')} />
        <Button title="To MemView again" onPress={() => this.props.navigation.push('MemGame')} />
        <Button title="To SvgView again" onPress={() => this.props.navigation.push('Game')} />

        <Header />

        <View style={styles.body}>
          {this.renderRows.call(this)}
        </View>

        <Score score={this.state.score} />

        <View style={styles.resetBtn}>
          <Button title="Reset" onPress={this.resetCards.bind(this)} color="#008CFA" />
        </View>
      </View>
    );
  }

  renderRows() {
    let rows = this.getRowContents(this.state.cards);

    return rows.map((row, index) => {
      return (
        <View key={index} style={styles.row}>
          {this.renderCards(row)}
        </View>
      );
    });
  }

  renderCards(row) {
    return row.map((card, index) => {
      return <Card
        key={index}
        src={card.src}
        name={card.name}
        color={card.color}
        isOpen={card.isOpen}
        clickCard={this.clickCard.bind(this, card.id)}
      />
    });
  }

  getRowContents(cards) {
    let rowContents = [];
    let contents = [];
    let count = 0;

    cards.forEach(card => {
      count++;
      contents.push(card);

      if (count === 4) {
        rowContents.push(contents);
        contents = [];
        count = 0;
      }
    });

    return rowContents;
  }

  resetCards() {
    let cards = this.cards.map(card => {
      card.isOpen = false;
      return card;
    })

    cards = cards.shuffle();

    this.setState({
      currentSelection: [],
      selectedPairs: [],
      cards: cards,
      score: 0,
    });
  }

  clickCard(id) {
    let currentSelection = this.state.currentSelection;
    let selectedPairs = this.state.selectedPairs;
    let score = this.state.score;

    let index = this.state.cards.findIndex(card => card.id === id)

    let cards = this.state.cards;

    // the card shouldn't already be opened and is not on the array of cards whose pairs are already selected
    if (cards[index].isOpen === false && selectedPairs.indexOf(cards[index].name) === -1) {
      cards[index].isOpen = true;

      currentSelection.push({
        name: cards[index].name,
        index,
      });

      if (currentSelection.length === 2) {
        if (currentSelection[0].name === currentSelection[1].name) {
          selectedPairs.push(cards[index].name);
          score++;
        } else {
          cards[currentSelection[0].index].isOpen = false;
          setTimeout(() => {
            cards[index].isOpen = false;
            this.setState({ cards });
          }, 500)
        }

        currentSelection = [];
      }

      this.setState({ cards, score, currentSelection });
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#fff',
  },

  body: {
    flex: 10,
    justifyContent: 'space-between',
    padding: 10,
    paddingTop: 20,
  },

  row: {
    flex: 1,
    flexDirection: 'row',
  },

  resetBtn: {
    flex: 1,
    flexDirection: 'column',
  }
});
