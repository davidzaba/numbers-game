import React from 'react';
import { reaction } from 'mobx';
import { inject, observer } from 'mobx-react/native';
import { StyleSheet, Text, View, Animated } from 'react-native';

import Countdown from './Countdown'
import GameOver from './GameOver'
import Numbers from './Numbers'
import Loader from './Loader'
import Choice from './Choice'
import Mask from './Mask'

import { Num } from './Constants/Num'

import DummyHeader from './Header/dummy'
import SuccessAnim from './Result/SuccessAnim'

/**
 *
 */
@inject('gameStore', 'maskStore', 'choicesStore', 'layoutStore', 'prohibitionStore') @observer
export default class Game extends React.Component {

  constructor (props) {
    super(props);

    this.screenWidth = this.screenHeight = 0
  }

  componentWillUnmount() {
    // discard observable reaction
    this.choicesObserver()
    this.exitGameObserver()
    this.sameSelectionsObserver()
    this.refreshChoicesObserver()

    this.endGame()
    this.playAgain()
  }

  render() {
    return (
      <View style={styles.container}>
        <DummyHeader />

        <View style={styles.mainArea} onLayout={this.onLayout.bind(this)}>
          {/* Circular time countdown */}
          <Countdown
            endGame={this.endGame.bind(this)} />

          {/* Top numeric container */}
          {
            !this.props.gameStore.gameOver && <Numbers groupId={Num.TOP_AREA} delay={200} />
          }

          {/* Numeric choice to start the game */}
          {
            !this.props.gameStore.gameOver && <Choice />
          }

          {/* Bottom numeric container */}
          {
            !this.props.gameStore.gameOver && <Numbers groupId={Num.BOTTOM_AREA} delay={500} />
          }

          {/* Game Over Screen */}
          {
            this.props.gameStore.gameOver && <GameOver playAgain={this.playAgain.bind(this)} />
          }

          {/* Mask to disable interactivity */}
          {
            this.props.maskStore.masking && <Mask bgColor="transparent" />
          }

          {/* Animation representing successful selection */}
          {
            this.props.gameStore.successAnimationVisible &&
            <SuccessAnim screenWidth={this.screenWidth} screenHeight={this.screenHeight} />
          }
        </View>
        {/* Loader */}
        {
          this.props.gameStore.loader &&
          <Loader />
        }

        {/* Mask to disable numeric selection */}
        {
          this.props.prohibitionStore.isInProhibitedState && <Mask bgColor="red" />
        }
      </View>
    );
  }

  onLayout(e) {
    let {width, height} = e.nativeEvent.layout

    this.screenWidth = width
    this.screenHeight = height
  }

  startGame() {
    this.props.gameStore.disableGameOver()
  }

  endGame() {
    this.props.gameStore.saveGame()
    this.props.gameStore.setGameOver()
    this.props.layoutStore.setGameOver()
    this.props.choicesStore.setGameOver()
    this.props.prohibitionStore.setGameOver()
  }

  playAgain() {
    this.startGame()
    this.props.gameStore.setReplay(true)
  }

  /**
   * observable reactions
   */
  choicesObserver = reaction(
    () => this.props.choicesStore.loadedChoices,
    () => {
      this.props.choicesStore.choicesAreReady && (() => {
        this.startGame()
        this.props.maskStore.deactivate()
      })()
    }
  )

  exitGameObserver = reaction(
    () => this.props.gameStore.exit,
    () => {
      this.props.navigation.navigate('Menu')
    }
  )

  sameSelectionsObserver = reaction(
    () => this.props.gameStore.hasMatchingSelections,
    (isMatch) => {
      isMatch.result && this.props.prohibitionStore.enableProhibitedState()
    }
  )

  refreshChoicesObserver = reaction(
    () => this.props.choicesStore.choicesAreRefreshed,
    (refreshed) => {
      refreshed &&
      this.props.choicesStore.refreshingChoices &&
      this.props.choicesStore.toggleRefreshingChoices();
    }
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  mainArea: {
    flex: 1,
  },
});
