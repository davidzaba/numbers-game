export const FontSeries = {
  condensed: {
    'EncodeSansCondensed-Regular': require('../assets/fonts/EncodeSansCondensed-Regular.ttf'),
    'EncodeSansCondensed-Black': require('../assets/fonts/EncodeSansCondensed-Black.ttf'),
    'EncodeSansCondensed-Thin': require('../assets/fonts/EncodeSansCondensed-Thin.ttf'),
  },
}
