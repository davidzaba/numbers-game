export const Choices = [
  {
    top: 10,
    color: '#0074D9',
    duration: 20000,
    statsCircleSize: 23,
  },

  {
    top: 30,
    color: '#3D9970',
    duration: 40000,
    statsCircleSize: 25,
  },

  {
    top: 70,
    color: '#85144b',
    duration: 60000,
    statsCircleSize: 27,
  },

  {
    top: 90,
    color: '#B10DC9',
    duration: 80000,
    statsCircleSize: 29,
  },
]
