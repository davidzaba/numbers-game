/**
 *
 * @type {[*]}
 */
export const AudioFiles = {
  BTN_PRESS: require('../assets/sounds/btnPress.mp3'),
  CIRCLE_INFLATE: require('../assets/sounds/circleInflate.mp3'),
  CIRCLE_DEFLATE: require('../assets/sounds/circleDeflate.mp3'),
}
