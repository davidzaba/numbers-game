/**
 *
 * @type {[*]}
 */
export const Alert = {
  Boundaries: [130, 30],

  Colors: {
    NORMAL: { idx: 0, val:'rgba(1, 255, 112, 1)' },
    WARNING: { idx: 1, val: 'rgba(255, 133, 27, 1)' },
    CRITICAL: { idx: 2, val: 'rgba(255, 65, 54, 1)' },
  }
}
