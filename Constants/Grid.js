export const Grid = {
  cellSize: 100,

  breakpoints: {
    width: [400, 500, 600, 700, 800, 900, 1000],
    height: [300, 400, 500, 600, 700, 800, 900],
  },
}
