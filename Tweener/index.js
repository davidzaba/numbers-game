import React from 'react';
import { Audio } from 'expo';
import { reaction } from 'mobx';
import { inject, observer } from 'mobx-react/native';
import { Feather, Ionicons } from '@expo/vector-icons';
import AnimateNumber from 'react-native-animate-number';
import { moderateScale, ScaledSheet } from 'react-native-size-matters';
import { StyleSheet, View, Text, TouchableHighlight } from 'react-native';

import { AudioFiles } from '../Constants/AudioFiles'
import RatingStars from '../Rating/Stars'
import Try from './Try'

/**
 *
 */
@inject('gameStore', 'choicesStore', 'prohibitionStore') @observer
export default class Tween extends React.Component {

  constructor (props) {
    super(props)

    this.touchableBgColor = 'rgba(138, 123, 77, 0.2)'
    this.tween = Math.round(Math.random() * this.props.maxTween)

    this.state = { btnSound: new Audio.Sound()}
  }

  async componentWillMount() {
    try {
      await Audio.setIsEnabledAsync(true)
      await this.state.btnSound.loadAsync(AudioFiles.BTN_PRESS)
    } catch (e) {}
  }

  async componentWillUnmount() {
    // discard observable reaction
    this.refreshChoicesObserver()

    // discard sound assets
    if (this.state.btnSound) {
      await this.state.btnSound.unloadAsync()
      this.state.btnSound = null
    }
  }

  render() {
    return (
      <View style={styles.tweenContainer}>
        {/* Left side */}
        <View style={styles.side}>
          {/* Tweened Number */}
          <AnimateNumber
            style={styles.tween}
            value={this.tween}
            timing='linear'
            onFinish={this.makeTweenReady.bind(this)}
            onProgress={(val) => null}
            formatter={(val) => Number(val).toFixed(0)} />

          {/* Score */}
          <View style={styles.ratingContainer}>
            <RatingStars />
          </View>
        </View>

        {/* Middle */}
        <View style={styles.middle}>
          {/* Verification Button */}
          {
            this.props.gameStore.isInVerifiableState &&
            !this.props.gameStore.isPaused &&
            <Try />
          }
        </View>

        {/* Right side */}
        <View style={styles.side}>
          {/* Refresh Button */}
          {
            !this.props.gameStore.isPaused &&
            <TouchableHighlight
              style={styles.refresh}
              underlayColor={this.touchableBgColor}
              onPressIn={this.playBtn.bind(this)}
              onPress={this.refreshChoices.bind(this)}>

              <View style={styles.refreshContainer}>
                <Text style={styles.refreshTxt}>{this.props.choicesStore.stats.currentSum}</Text>
                <Feather size={moderateScale(30)} name={'refresh-cw'} color={'black'} />
              </View>
            </TouchableHighlight>
            || <View style={styles.refresh} />
          }

          {/* Exit Button */}
          {
            !this.props.gameStore.isPaused &&
            <TouchableHighlight
              style={styles.exit}
              underlayColor={this.touchableBgColor}
              onPressIn={this.playBtn.bind(this)}
              onPress={this.exitGame.bind(this)}>

              <View style={styles.exitContainer}>
                <Ionicons size={moderateScale(40)} name={'ios-exit-outline'} color={'black'} />
              </View>
            </TouchableHighlight>
            || <View style={styles.exit} />
          }

          {/* Play / Pause Button */}
          <TouchableHighlight
            style={styles.playPause}
            underlayColor={this.touchableBgColor}
            onPressIn={this.playBtn.bind(this)}
            onPress={this.playPause.bind(this)}>

            <View style={styles.playPauseContainer}>
              <Feather size={moderateScale(30)} name={this.props.gameStore.isPaused ? 'play' : 'pause'} color={'black'} />
            </View>
          </TouchableHighlight>
        </View>
      </View>
    );
  }

  makeTweenReady() {
    this.props.gameStore.makeTweenReady()
    this.props.gameStore.tween = this.tween
  }

  refreshChoices() {
    if (this.props.gameStore.hasSelections) {
      this.props.prohibitionStore.enableProhibitedState()
      return
    }

    if (this.props.gameStore.isResolvingSelection) {
      this.props.prohibitionStore.enableProhibitedState()
      return
    }

    if (this.props.choicesStore.refreshingChoices) {
      this.props.prohibitionStore.enableProhibitedState()
      return
    }

    this.props.choicesStore.downgradeRefreshChoices()

    !this.props.choicesStore.refreshChoicesDepleted && (() => {
      this.props.choicesStore.toggleRefreshingChoices();
      this.props.choicesStore.resetRefreshedChoices();

    })()
  }

  exitGame() {
    this.props.gameStore.exitGame()
  }

  playPause() {
    if (this.props.choicesStore.refreshingChoices) {
      this.props.prohibitionStore.enableProhibitedState()
      return
    }

    this.props.gameStore.playPause()
  }

  async playBtn() {
    try {
      await this.state.btnSound.setOnPlaybackStatusUpdate(null)
      await this.state.btnSound.setPositionAsync(0)
      await this.state.btnSound.setVolumeAsync(1)
      await this.state.btnSound.playAsync()
    } catch (e) {}
  }

  /**
   * observable reactions
   */
  refreshChoicesObserver = reaction(
    () => this.props.choicesStore.stats,
    () => {
      this.props.choicesStore.refreshChoicesDepleted && this.props.prohibitionStore.enableProhibitedState()
    }
  )
}

/**
 * Styles
 */
const styles = ScaledSheet.create({
  tweenContainer: {
    flex: 1,
    flexDirection: 'row',
  },

  side: {
    flex: 2,
    flexDirection: 'row',
  },

  middle: {
    flex: 1,
    backgroundColor: 'transparent',
  },

  verify: {
    flex: 1,
    backgroundColor: 'white'
  },

  ratingContainer: {
    flex: 1,
  },

  score: {
    flex: 1,
    backgroundColor: 'green'
  },

  tween: {
    flex: 1,
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: "45@s",
    color: '#001f3f',
    paddingBottom: 5,
    fontFamily: 'EncodeSansCondensed-Black',
  },

  refreshContainer: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  refresh: {
    flex: 1,
  },

  refreshTxt: {
    position: 'absolute',
    fontSize: "10@s",
  },

  exitContainer: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  exit: {
    flex: 1,
  },

  playPauseContainer: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  playPause: {
    flex: 1,
  },
});
