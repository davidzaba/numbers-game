import React from 'react';
import { Svg } from 'expo';
import { inject } from 'mobx-react/native';
import { MaterialIcons } from '@expo/vector-icons';
import { moderateScale } from 'react-native-size-matters';
import { StyleSheet, View, Animated, TouchableHighlight } from 'react-native';

import { CircleUtils } from '../services/Circle'

// Svg
const { Circle } = Svg
const TryCircle = Animated.createAnimatedComponent(Circle)

/**
 *
 */
@inject('gameStore')
export default class Try extends React.Component {

  constructor (props) {
    super(props)

    this.state = { layout: false }

    this.width = 0
    this.height = 0

    this.tryCircle = null
    this.tryLeftOffset = 0

    this.radius = this.cx = this.cy = this.size = 0
  }

  render() {
    return (
      <TouchableHighlight
        style={styles.container}
        onLayout={this.onLayout.bind(this)}
        onPress={this.verifySelections.bind(this)}>

        <View style={styles.tryContainer}>
          {
            this.state.layout &&
            <Svg width={this.size} height={this.size} style={[styles.svgContainer, {left: this.tryLeftOffset}]}>
              <TryCircle
                ref={(component) => this.tryCircle = component}
                cx={this.cx}
                cy={this.cy}
                r={this.radius}
                fill='#001f3f' />
            </Svg>
          }
          <MaterialIcons size={moderateScale(40)} name={'touch-app'} color={'white'} />
        </View>
      </TouchableHighlight>
    );
  }

  onLayout(e) {
    if (this.cx && this.cy) {
      return
    }

    let { width, height } = e.nativeEvent.layout

    this.makeGrid(width, height)
  }

  makeGrid(width, height) {
    let utils = new CircleUtils()
    utils.build(width, height, 5)

    this.size = utils.getSize()

    this.cx = utils.getCX()
    this.cy = utils.getCY()

    this.radius = utils.getRadius();

    this.tryLeftOffset = (width - this.size) / 2

    this.setState({ layout: true })
  }

  verifySelections() {
    this.props.gameStore.verifySelections()
  }
}

/**
 * Styles
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  tryContainer: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  svgContainer: {
    flex: 1,
    position: 'absolute',
    top: 0,
    left: 0,
  },
});
