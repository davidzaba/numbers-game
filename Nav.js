import React from 'react';
import { Provider } from 'mobx-react'
import { createStackNavigator } from 'react-navigation';

import Stats from './Stats'
import Menu from './Menu'
import Game from './Game'

import ProhibitionStore from './Store/Prohibition'
import ChoicesStore from './Store/Choices'
import LayoutStore from './Store/Layout'
import GameStore from './Store/Game'
import MaskStore from './Store/Mask'

/**
 *
 * @type {object}
 */
export const Routes = {
  stats: 'Stats',
  menu: 'Menu',
  game: 'Game',
}

/**
 * Navigator
 */
const StackNav = createStackNavigator(
  {
    Menu,
    Game,
    Stats,
  },

  {
    initialRouteName: Routes.menu,
    headerMode: 'none',

    navigatorOptions: {
      headerVisible: false
    }
  }
);

/**
 * Nav
 */
export default class Nav extends React.Component {

  constructor (props) {
    super(props)

    this.stores = {
      maskStore: MaskStore,
      gameStore: GameStore,
      layoutStore: LayoutStore,
      choicesStore: ChoicesStore,
      prohibitionStore: ProhibitionStore,
    }
  }

  render() {

    return (
      <Provider {...this.stores}>
        <StackNav />
      </Provider>
    )
  }
}
