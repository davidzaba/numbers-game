import React from 'react';
import { reaction } from 'mobx';
import { Svg, Audio } from 'expo';
import { inject, observer } from 'mobx-react/native';
import extractBrush from 'react-native-svg/lib/extract/extractBrush';
import { StyleSheet, View, Animated, Dimensions } from 'react-native';

import { Timing } from './Animations/Timing'

import { State as AlertState } from './Alert/State'
import { Alert } from './Constants/Alert'

import { AudioFiles } from './Constants/AudioFiles'
import { Config } from './Constants/Config'

// SVG
const { Circle } = Svg;
const CountdownCircle = Animated.createAnimatedComponent(Circle)

/**
 *
 */
@inject('gameStore', 'choicesStore') @observer
export default class Coundown extends React.Component {

  constructor (props) {
    super(props);

    this.state = {
      cirInflateSound: new Audio.Sound(),
      cirDeflateSound: new Audio.Sound(),
    }

    this.isInflating = true

    // screen dimensions
    let { width } = Dimensions.get('screen')
    this.countdownCircleWidth = Math.floor(width)

    // radius computations
    this.fullRadius = this.countdownCircleWidth / 2
    this.circleX = this.circleY = this.fullRadius

    this.setupCircleAnimation()

    // reference to animated circles
    this.circle = null

    this.deflatingAnim = null
    this.currentCircleSize = 0

    this.remainingGameTime = 0

    this.circleBgColor = { idx: 0, val: Alert.Colors.NORMAL.val }
    this.colorFill = new Animated.Value(0)
    this.bgColor = Alert.Colors.NORMAL.val
    this.isCircleBgColorAnimated = false
  }

  async componentWillMount() {
    try {
      await Audio.setIsEnabledAsync(true)
      await this.state.cirInflateSound.loadAsync(AudioFiles.CIRCLE_INFLATE)
      await this.state.cirDeflateSound.loadAsync(AudioFiles.CIRCLE_DEFLATE)
    } catch (e) {}
  }

  componentDidMount() {
    this.inflateCircularCountdown()
  }

  async componentWillUnmount() {
    // discard observable reaction
    this.jumpObserver()
    this.replayObserver()
    this.choicesObserver()
    this.playPauseObserver()
    this.refreshingChoicesObserver()

    this.countdownCircleAnim.r.removeAllListeners()

    // discard sound assests
    if (!this.state.cirInflateSound) {
      await this.state.cirInflateSound.unloadAsync()
      this.state.cirInflateSound = null
    }

    if (!this.state.cirDeflateSound) {
      await this.state.cirDeflateSound.unloadAsync()
      this.state.cirDeflateSound = null
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Svg width={this.countdownCircleWidth} height={this.countdownCircleWidth} style={styles.svg}>
          {/* Main graphical countdown */}
          <CountdownCircle
            ref={(component) => !this.circle && (this.circle = component)}
            cx={this.circleX}
            cy={this.circleY}
            stroke='white'
            strokeWidth='1'
            fill={this.bgColor} />

          {/* Circular time alert boundaries */}
          {Alert.Boundaries.map((gauge) =>
            <Circle
              key={gauge}
              cx={this.circleX}
              cy={this.circleY}
              r={gauge}
              stroke='white'
              strokeWidth='1'
              fill='transparent' />
          )}
        </Svg>
      </View>
    );
  }

  deflateCircularCountdown() {
    this.isInflating = false

    this.deflatingAnim = Animated.sequence([
      (new Timing(this.remainingGameTime, false))
        .start(this.countdownCircleAnim.r, 0, 0, true)
    ])

    this.deflatingAnim.start()
  }

  inflateCircularCountdown(delay = 0) {
    (new Timing(Config.RESET_DURATION))
      .start(this.countdownCircleAnim.r, this.fullRadius, delay + 500, true)()
  }

  setupCircleAnimation() {
    // reset all possible running animations
    this.countdownCircleAnim &&
    this.countdownCircleAnim.r &&
    this.countdownCircleAnim.r.removeAllListeners()

    this.countdownCircleAnim = { r: new Animated.Value(0) }

    // regularly update to animate
    this.countdownCircleAnim.r.addListener((radius) => {
      // do not proceed if game is halted
      if (this.props.gameStore.isPaused || this.props.choicesStore.refreshingChoices) {
        this.setRemainingGameTime(radius.value)
        this.deflatingAnim.stop()

        return
      }

      let val = radius.value
      this.currentCircleSize = val

      !this.isInflating && this.transitionBgColor(val)

      // update circle size
      this.circle.setNativeProps({ r: val.toString() })

      !this.props.gameStore.gameOver && !val && !this.isInflating && this.props.endGame()
    })
  }

  jumpCircularCountdown(punish) {
    // cease current countdown animation
    this.deflatingAnim.stop();

    let currentRadius = Number(this.currentCircleSize.toFixed(0))
    let newRadius

    // get radius to inflate or deflate countdown
    if (punish) {
      newRadius = Math.max(0, currentRadius - Config.JUMP_SIZE);
      this.playCircleJump(false)
    } else {
      newRadius = Math.min(this.fullRadius, currentRadius + Config.JUMP_SIZE);
      this.props.choicesStore.upgradeRefreshChoices()
      this.playCircleJump(true)
    }

    // deflate rapidly and resume standard countdown
    (new Timing(300))
      .start(this.countdownCircleAnim.r, newRadius, 0, true)(
        () => this.deflateCircularCountdown()
      )
  }

  setRemainingGameTime(currVal) {
    this.remainingGameTime = Number((this.props.gameStore.selectedDuration * currVal) / this.fullRadius)
  }

  async playCircleJump(inflate = true) {
    try {
      if (inflate) {
        await this.state.cirInflateSound.setPositionAsync(0)
        await this.state.cirInflateSound.setVolumeAsync(1)
        await this.state.cirInflateSound.playAsync()
      } else {
        await this.state.cirDeflateSound.setPositionAsync(0)
        await this.state.cirDeflateSound.setVolumeAsync(1)
        await this.state.cirDeflateSound.playAsync()
      }
    } catch (e) {
      console.log(e)
    }
  }

  transitionBgColor(circleSize) {
    let colorFill = AlertState.getInflatingColor(circleSize)

    if (colorFill.idx === this.circleBgColor.idx) {
      return
    }

    if (this.isCircleBgColorAnimated) {
      return
    }

    this.isCircleBgColorAnimated = true

    this.colorFill = new Animated.Value(0)

    // interpolation to prep bg color transition
    this.bgColor = this.colorFill.interpolate({
      inputRange: [0, 1],
      outputRange: [this.circleBgColor.val, colorFill.val]
    });

    // listener to apply bg color
    this.colorFill.removeAllListeners()
    this.colorFill.addListener(() => {
      try {
        let fill = this.bgColor.__getValue()
        fill && this.circle.setNativeProps({ fill: extractBrush(fill) })
      } catch(e) {}
    });

    // start transition
    (new Timing(1000)).start(this.colorFill, 1, 0, true)(
      () => { this.resetBgColorTransition(colorFill) }
    )
  }

  resetBgColorTransition(col) {
    this.isCircleBgColorAnimated = false
    this.circleBgColor = col
    this.bgColor = col.val
  }

  /**
   * observable reactions
   */
  replayObserver = reaction(
    () => this.props.gameStore.replay,
    (replay) => {
      replay && (() => {
        this.isInflating = true

        this.setupCircleAnimation();
        this.inflateCircularCountdown(200)
        this.resetBgColorTransition(Alert.Colors.NORMAL)

        this.props.gameStore.setReplay(false)
      })()
    }
  )

  choicesObserver = reaction(
    () => this.props.choicesStore.loadedChoices,
    () => {
      this.props.choicesStore.choicesAreReady && (() => {
        this.setRemainingGameTime(this.fullRadius)
        this.deflateCircularCountdown()
      })()
    }
  )

  jumpObserver = reaction(
    () => this.props.gameStore.isJumping,
    (isJumping) => {
      isJumping && this.jumpCircularCountdown(isJumping.punish)
    }
  )

  playPauseObserver = reaction(
    () => this.props.gameStore.isPaused,
    (isPaused) => {
      !isPaused && this.deflateCircularCountdown()
    }
  )

  refreshingChoicesObserver = reaction(
    () => this.props.choicesStore.refreshingChoices,
    (refreshing) => {
      !refreshing && this.deflateCircularCountdown()
    }
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
