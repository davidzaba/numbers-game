import { CircleUtils } from '../services/Circle'

test('CircleUtils Service', () => {
  [
    // test set
    [
      // DATA IN
      true,

      20, // width
      30, // height
      0, // offset from rim

      // DATA OUT
      20, // size
      10, // cx
      10, // cy
      10, // radius
    ],

    // test set
    [
      // DATA IN
      true,

      50, // width
      60, // height
      0, // offset from rim

      // DATA OUT
      50, // size
      25, // cx
      25, // cy
      25, // radius
    ],

    // test set
    [
      // DATA IN
      true,

      53, // width
      60, // height
      0, // offset from rim

      // DATA OUT
      53, // size
      26.5, // cx
      26.5, // cy
      26.5, // radius
    ],

    // test set
    [
      // DATA IN
      false,

      20, // width
      30, // height
      0, // offset from rim

      // DATA OUT
      20, // size
      10, // cx
      15, // cy
      10, // radius
    ],

    // test set
    [
      // DATA IN
      false,

      50, // width
      60, // height
      0, // offset from rim

      // DATA OUT
      50, // size
      25, // cx
      30, // cy
      25, // radius
    ],

    // test set
    [
      // DATA IN
      false,

      53, // width
      60, // height
      0, // offset from rim

      // DATA OUT
      53, // size
      26.5, // cx
      30, // cy
      26.5, // radius
    ],
  ].forEach((testSet) => {
    const CircUtils = new CircleUtils(/*isEqual*/testSet[0])

    CircUtils.build(/*width*/testSet[1], /*height*/testSet[2], /*offset*/testSet[3])

    expect(CircUtils.getRadius()).toEqual(testSet[7])
    expect(CircUtils.getSize()).toEqual(testSet[4])

    expect(CircUtils.getCX()).toEqual(testSet[5])
    expect(CircUtils.getCY()).toEqual(testSet[6])
  })
})
