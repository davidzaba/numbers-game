import { RandomFromRange } from '../services/Randomizer'

test('Randomizer Service', () => {
  [
    // test set
    [
      // DATA IN
      0, // min
      10, // max
      0, // toFixed

      // DATA OUT
      0, // >=
      10, // <=
    ],

    // test set
    [
      // DATA IN
      5, // min
      6, // max
      0, // toFixed

      // DATA OUT
      4, // >=
      6, // <=
    ],

    // test set
    [
      // DATA IN
      0, // min
      10, // max
      5, // toFixed

      // DATA OUT
      0, // >=
      10, // <=
    ],

    // test set
    [
      // DATA IN
      5, // min
      6, // max
      10, // toFixed

      // DATA OUT
      4, // >=
      6, // <=
    ],
  ].forEach((testSet) => {
    expect(RandomFromRange.make(/*min*/testSet[0], /*max*/testSet[1], /*toFixed*/testSet[2])).toBeGreaterThanOrEqual(testSet[3])
    expect(RandomFromRange.make(/*min*/testSet[0], /*max*/testSet[1], /*toFixed*/testSet[2])).toBeLessThanOrEqual(testSet[4])
  })
})
