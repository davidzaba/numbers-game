import { StorageManager } from '../Storage/Manager'

test('Storage Module - Single Entry', () => {
  [
    // test set
    [
      // DATA IN

      // entry args
      [/*range*/10, /*score*/20, /*stars*/0, /*touches*/30],

      // saved stats
      '[]',

      // DATA OUT
      [
        {
          "range": 10,
          "score": 20,
          "title": "SCORE: 20",
          "description": "Range: 10 Stars: 0 Touches: 30",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
      ],
    ],
  ].forEach((testSet) => {
    let args = testSet[0]
    let savedStats = testSet[1]
    let expectedResult = testSet[2]

    const Manager = new StorageManager(/*range*/args[0], /*score*/args[1], /*stars*/args[2], /*touches*/args[3])

    Manager.init()
    Manager.setRangeItems(5)
    Manager.setTotalItems(20)

    expect(Manager.sort(savedStats)).toEqual(expectedResult)
  })
})

test('Storage Module - Multiple Entries with Same Range', () => {
  [
    // test set
    [
      // DATA IN

      // entry args
      [/*range*/10, /*score*/30, /*stars*/5, /*touches*/40],

      // saved stats
      `[
        {
          "range": 10,
          "score": 20,
          "title": "SCORE: 20",
          "description": "Range: 10 Stars: 0 Touches: 30",
          "circleSize": 23,
          "circleColor": "#0074D9"
        }
      ]`,

      // DATA OUT
      [
        {
          range: 10,
          score: 30,
          title: 'SCORE: 30',
          description: 'Range: 10 Stars: 5 Touches: 40',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 20,
          title: 'SCORE: 20',
          description: 'Range: 10 Stars: 0 Touches: 30',
          circleSize: 23,
          circleColor: '#0074D9'
        },
      ],
    ],

    // test set
    [
      // DATA IN

      // entry args
      [/*range*/10, /*score*/10, /*stars*/0, /*touches*/5],

      // saved stats
      `[
        {
          "range": 10,
          "score": 30,
          "title": "SCORE: 30",
          "description": "Range: 10 Stars: 5 Touches: 40",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },

        {
          "range": 10,
          "score": 20,
          "title": "SCORE: 20",
          "description": "Range: 10 Stars: 0 Touches: 30",
          "circleSize": 23,
          "circleColor": "#0074D9"
        }
      ]`,

      // DATA OUT
      [
        {
          range: 10,
          score: 30,
          title: 'SCORE: 30',
          description: 'Range: 10 Stars: 5 Touches: 40',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 20,
          title: 'SCORE: 20',
          description: 'Range: 10 Stars: 0 Touches: 30',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 10,
          title: 'SCORE: 10',
          description: 'Range: 10 Stars: 0 Touches: 5',
          circleSize: 23,
          circleColor: '#0074D9'
        },
      ],
    ],

    // test set
    [
      // DATA IN

      // entry args
      [/*range*/10, /*score*/50, /*stars*/10, /*touches*/25],

      // saved stats
      `[
        {
          "range": 10,
          "score": 30,
          "title": "SCORE: 30",
          "description": "Range: 10 Stars: 5 Touches: 40",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },

        {
          "range": 10,
          "score": 20,
          "title": "SCORE: 20",
          "description": "Range: 10 Stars: 0 Touches: 30",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 10,
          "title": "SCORE: 10",
          "description": "Range: 10 Stars: 0 Touches: 5",
          "circleSize": 23,
          "circleColor": "#0074D9"
        }
      ]`,

      // DATA OUT
      [
        {
          range: 10,
          score: 50,
          title: 'SCORE: 50',
          description: 'Range: 10 Stars: 10 Touches: 25',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 30,
          title: 'SCORE: 30',
          description: 'Range: 10 Stars: 5 Touches: 40',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 20,
          title: 'SCORE: 20',
          description: 'Range: 10 Stars: 0 Touches: 30',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 10,
          title: 'SCORE: 10',
          description: 'Range: 10 Stars: 0 Touches: 5',
          circleSize: 23,
          circleColor: '#0074D9'
        },
      ],
    ],

    // test set
    [
      // DATA IN

      // entry args
      [/*range*/10, /*score*/15, /*stars*/2, /*touches*/7],

      // saved stats
      `[
        {
          "range": 10,
          "score": 50,
          "title": "SCORE: 50",
          "description": "Range: 10 Stars: 10 Touches: 25",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 30,
          "title": "SCORE: 30",
          "description": "Range: 10 Stars: 5 Touches: 40",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },

        {
          "range": 10,
          "score": 20,
          "title": "SCORE: 20",
          "description": "Range: 10 Stars: 0 Touches: 30",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 10,
          "title": "SCORE: 10",
          "description": "Range: 10 Stars: 0 Touches: 5",
          "circleSize": 23,
          "circleColor": "#0074D9"
        }
      ]`,

      // DATA OUT
      [
        {
          range: 10,
          score: 50,
          title: 'SCORE: 50',
          description: 'Range: 10 Stars: 10 Touches: 25',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 30,
          title: 'SCORE: 30',
          description: 'Range: 10 Stars: 5 Touches: 40',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 20,
          title: 'SCORE: 20',
          description: 'Range: 10 Stars: 0 Touches: 30',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 15,
          title: 'SCORE: 15',
          description: 'Range: 10 Stars: 2 Touches: 7',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 10,
          title: 'SCORE: 10',
          description: 'Range: 10 Stars: 0 Touches: 5',
          circleSize: 23,
          circleColor: '#0074D9'
        },
      ],
    ],

    // test set
    [
      // DATA IN

      // entry args
      [/*range*/10, /*score*/5, /*stars*/0, /*touches*/2],

      // saved stats
      `[
        {
          "range": 10,
          "score": 50,
          "title": "SCORE: 50",
          "description": "Range: 10 Stars: 10 Touches: 25",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 30,
          "title": "SCORE: 30",
          "description": "Range: 10 Stars: 5 Touches: 40",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },

        {
          "range": 10,
          "score": 20,
          "title": "SCORE: 20",
          "description": "Range: 10 Stars: 0 Touches: 30",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 15,
          "title": "SCORE: 15",
          "description": "Range: 10 Stars: 2 Touches: 7",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 10,
          "title": "SCORE: 10",
          "description": "Range: 10 Stars: 0 Touches: 5",
          "circleSize": 23,
          "circleColor": "#0074D9"
        }
      ]`,

      // DATA OUT
      [
        {
          range: 10,
          score: 50,
          title: 'SCORE: 50',
          description: 'Range: 10 Stars: 10 Touches: 25',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 30,
          title: 'SCORE: 30',
          description: 'Range: 10 Stars: 5 Touches: 40',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 20,
          title: 'SCORE: 20',
          description: 'Range: 10 Stars: 0 Touches: 30',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 15,
          title: 'SCORE: 15',
          description: 'Range: 10 Stars: 2 Touches: 7',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 10,
          title: 'SCORE: 10',
          description: 'Range: 10 Stars: 0 Touches: 5',
          circleSize: 23,
          circleColor: '#0074D9'
        },
      ],
    ],

    // test set
    [
      // DATA IN

      // entry args
      [/*range*/10, /*score*/12, /*stars*/1, /*touches*/6],

      // saved stats
      `[
        {
          "range": 10,
          "score": 50,
          "title": "SCORE: 50",
          "description": "Range: 10 Stars: 10 Touches: 25",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 30,
          "title": "SCORE: 30",
          "description": "Range: 10 Stars: 5 Touches: 40",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },

        {
          "range": 10,
          "score": 20,
          "title": "SCORE: 20",
          "description": "Range: 10 Stars: 0 Touches: 30",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 15,
          "title": "SCORE: 15",
          "description": "Range: 10 Stars: 2 Touches: 7",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 10,
          "title": "SCORE: 10",
          "description": "Range: 10 Stars: 0 Touches: 5",
          "circleSize": 23,
          "circleColor": "#0074D9"
        }
      ]`,

      // DATA OUT
      [
        {
          range: 10,
          score: 50,
          title: 'SCORE: 50',
          description: 'Range: 10 Stars: 10 Touches: 25',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 30,
          title: 'SCORE: 30',
          description: 'Range: 10 Stars: 5 Touches: 40',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 20,
          title: 'SCORE: 20',
          description: 'Range: 10 Stars: 0 Touches: 30',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 15,
          title: 'SCORE: 15',
          description: 'Range: 10 Stars: 2 Touches: 7',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 12,
          title: 'SCORE: 12',
          description: 'Range: 10 Stars: 1 Touches: 6',
          circleSize: 23,
          circleColor: '#0074D9'
        },
      ],
    ],

    // test set
    [
      // DATA IN

      // entry args
      [/*range*/10, /*score*/100, /*stars*/20, /*touches*/50],

      // saved stats
      `[
        {
          "range": 10,
          "score": 50,
          "title": "SCORE: 50",
          "description": "Range: 10 Stars: 10 Touches: 25",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 30,
          "title": "SCORE: 30",
          "description": "Range: 10 Stars: 5 Touches: 40",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },

        {
          "range": 10,
          "score": 20,
          "title": "SCORE: 20",
          "description": "Range: 10 Stars: 0 Touches: 30",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 15,
          "title": "SCORE: 15",
          "description": "Range: 10 Stars: 2 Touches: 7",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 12,
          "title": "SCORE: 12",
          "description": "Range: 10 Stars: 1 Touches: 6",
          "circleSize": 23,
          "circleColor": "#0074D9"
        }
      ]`,

      // DATA OUT
      [
        {
          range: 10,
          score: 100,
          title: 'SCORE: 100',
          description: 'Range: 10 Stars: 20 Touches: 50',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 50,
          title: 'SCORE: 50',
          description: 'Range: 10 Stars: 10 Touches: 25',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 30,
          title: 'SCORE: 30',
          description: 'Range: 10 Stars: 5 Touches: 40',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 20,
          title: 'SCORE: 20',
          description: 'Range: 10 Stars: 0 Touches: 30',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 15,
          title: 'SCORE: 15',
          description: 'Range: 10 Stars: 2 Touches: 7',
          circleSize: 23,
          circleColor: '#0074D9'
        },
      ],
    ],
  ].forEach((testSet) => {
    let args = testSet[0]
    let savedStats = testSet[1]
    let expectedResult = testSet[2]

    const Manager = new StorageManager(/*range*/args[0], /*score*/args[1], /*stars*/args[2], /*touches*/args[3])

    Manager.init()
    Manager.setRangeItems(5)
    Manager.setTotalItems(20)

    expect(Manager.sort(savedStats)).toEqual(expectedResult)
  })
})


test('Storage Module - Multiple Entries with Different Ranges', () => {
  [
    // test set
    [
      // DATA IN

      // entry args
      [/*range*/30, /*score*/10, /*stars*/1, /*touches*/5],

      // saved stats
      `[
        {
          "range": 10,
          "score": 100,
          "title": "SCORE: 100",
          "description": "Range: 10 Stars: 30 Touches: 50",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 50,
          "title": "SCORE: 50",
          "description": "Range: 10 Stars: 10 Touches: 25",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 30,
          "title": "SCORE: 30",
          "description": "Range: 10 Stars: 5 Touches: 40",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },

        {
          "range": 10,
          "score": 20,
          "title": "SCORE: 20",
          "description": "Range: 10 Stars: 0 Touches: 30",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 15,
          "title": "SCORE: 15",
          "description": "Range: 10 Stars: 2 Touches: 7",
          "circleSize": 23,
          "circleColor": "#0074D9"
        }
      ]`,

      // DATA OUT
      [
        {
          range: 10,
          score: 100,
          title: 'SCORE: 100',
          description: 'Range: 10 Stars: 30 Touches: 50',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 50,
          title: 'SCORE: 50',
          description: 'Range: 10 Stars: 10 Touches: 25',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 30,
          title: 'SCORE: 30',
          description: 'Range: 10 Stars: 5 Touches: 40',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 20,
          title: 'SCORE: 20',
          description: 'Range: 10 Stars: 0 Touches: 30',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 15,
          title: 'SCORE: 15',
          description: 'Range: 10 Stars: 2 Touches: 7',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 30,
          score: 10,
          title: 'SCORE: 10',
          description: 'Range: 30 Stars: 1 Touches: 5',
          circleSize: 25,
          circleColor: '#3D9970'
        },
      ],
    ],

    // test set
    [
      // DATA IN

      // entry args
      [/*range*/30, /*score*/22, /*stars*/1, /*touches*/10],

      // saved stats
      `[
        {
          "range": 30,
          "score": 120,
          "title": "SCORE: 120",
          "description": "Range: 30 Stars: 20 Touches: 60",
          "circleSize": 25,
          "circleColor": "#3D9970"
        },
        
        {
          "range": 10,
          "score": 100,
          "title": "SCORE: 100",
          "description": "Range: 10 Stars: 30 Touches: 50",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 30,
          "score": 70,
          "title": "SCORE: 70",
          "description": "Range: 30 Stars: 15 Touches: 35",
          "circleSize": 25,
          "circleColor": "#3D9970"
        },
        
        {
          "range": 10,
          "score": 50,
          "title": "SCORE: 50",
          "description": "Range: 10 Stars: 10 Touches: 25",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 30,
          "title": "SCORE: 30",
          "description": "Range: 10 Stars: 5 Touches: 40",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 30,
          "score": 25,
          "title": "SCORE: 25",
          "description": "Range: 30 Stars: 2 Touches: 10",
          "circleSize": 25,
          "circleColor": "#3D9970"
        },

        {
          "range": 10,
          "score": 20,
          "title": "SCORE: 20",
          "description": "Range: 10 Stars: 0 Touches: 30",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 15,
          "title": "SCORE: 15",
          "description": "Range: 10 Stars: 2 Touches: 7",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 30,
          "score": 10,
          "title": "SCORE: 10",
          "description": "Range: 30 Stars: 1 Touches: 5",
          "circleSize": 25,
          "circleColor": "#3D9970"
        }
      ]`,

      // DATA OUT
      [
        {
          range: 30,
          score: 120,
          title: 'SCORE: 120',
          description: 'Range: 30 Stars: 20 Touches: 60',
          circleSize: 25,
          circleColor: '#3D9970'
        },

        {
          range: 10,
          score: 100,
          title: 'SCORE: 100',
          description: 'Range: 10 Stars: 30 Touches: 50',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 30,
          score: 70,
          title: 'SCORE: 70',
          description: 'Range: 30 Stars: 15 Touches: 35',
          circleSize: 25,
          circleColor: '#3D9970'
        },

        {
          range: 10,
          score: 50,
          title: 'SCORE: 50',
          description: 'Range: 10 Stars: 10 Touches: 25',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 30,
          title: 'SCORE: 30',
          description: 'Range: 10 Stars: 5 Touches: 40',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 30,
          score: 25,
          title: 'SCORE: 25',
          description: 'Range: 30 Stars: 2 Touches: 10',
          circleSize: 25,
          circleColor: '#3D9970'
        },

        {
          range: 30,
          score: 22,
          title: 'SCORE: 22',
          description: 'Range: 30 Stars: 1 Touches: 10',
          circleSize: 25,
          circleColor: '#3D9970'
        },

        {
          range: 10,
          score: 20,
          title: 'SCORE: 20',
          description: 'Range: 10 Stars: 0 Touches: 30',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 15,
          title: 'SCORE: 15',
          description: 'Range: 10 Stars: 2 Touches: 7',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 30,
          score: 10,
          title: 'SCORE: 10',
          description: 'Range: 30 Stars: 1 Touches: 5',
          circleSize: 25,
          circleColor: '#3D9970'
        },
      ],
    ],

    // test set
    [
      // DATA IN

      // entry args
      [/*range*/30, /*score*/5, /*stars*/0, /*touches*/2],

      // saved stats
      `[
        {
          "range": 30,
          "score": 120,
          "title": "SCORE: 120",
          "description": "Range: 30 Stars: 20 Touches: 60",
          "circleSize": 25,
          "circleColor": "#3D9970"
        },
        
        {
          "range": 10,
          "score": 100,
          "title": "SCORE: 100",
          "description": "Range: 10 Stars: 30 Touches: 50",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 30,
          "score": 70,
          "title": "SCORE: 70",
          "description": "Range: 30 Stars: 15 Touches: 35",
          "circleSize": 25,
          "circleColor": "#3D9970"
        },
        
        {
          "range": 10,
          "score": 50,
          "title": "SCORE: 50",
          "description": "Range: 10 Stars: 10 Touches: 25",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 30,
          "title": "SCORE: 30",
          "description": "Range: 10 Stars: 5 Touches: 40",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 30,
          "score": 25,
          "title": "SCORE: 25",
          "description": "Range: 30 Stars: 2 Touches: 10",
          "circleSize": 25,
          "circleColor": "#3D9970"
        },
        
        {
          "range": 30,
          "score": 22,
          "title": "SCORE: 22",
          "description": "Range: 30 Stars: 1 Touches: 10",
          "circleSize": 25,
          "circleColor": "#3D9970"
        },

        {
          "range": 10,
          "score": 20,
          "title": "SCORE: 20",
          "description": "Range: 10 Stars: 0 Touches: 30",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 10,
          "score": 15,
          "title": "SCORE: 15",
          "description": "Range: 10 Stars: 2 Touches: 7",
          "circleSize": 23,
          "circleColor": "#0074D9"
        },
        
        {
          "range": 30,
          "score": 10,
          "title": "SCORE: 10",
          "description": "Range: 30 Stars: 1 Touches: 5",
          "circleSize": 25,
          "circleColor": "#3D9970"
        }
      ]`,

      // DATA OUT
      [
        {
          range: 30,
          score: 120,
          title: 'SCORE: 120',
          description: 'Range: 30 Stars: 20 Touches: 60',
          circleSize: 25,
          circleColor: '#3D9970'
        },

        {
          range: 10,
          score: 100,
          title: 'SCORE: 100',
          description: 'Range: 10 Stars: 30 Touches: 50',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 30,
          score: 70,
          title: 'SCORE: 70',
          description: 'Range: 30 Stars: 15 Touches: 35',
          circleSize: 25,
          circleColor: '#3D9970'
        },

        {
          range: 10,
          score: 50,
          title: 'SCORE: 50',
          description: 'Range: 10 Stars: 10 Touches: 25',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 30,
          title: 'SCORE: 30',
          description: 'Range: 10 Stars: 5 Touches: 40',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 30,
          score: 25,
          title: 'SCORE: 25',
          description: 'Range: 30 Stars: 2 Touches: 10',
          circleSize: 25,
          circleColor: '#3D9970'
        },

        {
          range: 30,
          score: 22,
          title: 'SCORE: 22',
          description: 'Range: 30 Stars: 1 Touches: 10',
          circleSize: 25,
          circleColor: '#3D9970'
        },

        {
          range: 10,
          score: 20,
          title: 'SCORE: 20',
          description: 'Range: 10 Stars: 0 Touches: 30',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 10,
          score: 15,
          title: 'SCORE: 15',
          description: 'Range: 10 Stars: 2 Touches: 7',
          circleSize: 23,
          circleColor: '#0074D9'
        },

        {
          range: 30,
          score: 10,
          title: 'SCORE: 10',
          description: 'Range: 30 Stars: 1 Touches: 5',
          circleSize: 25,
          circleColor: '#3D9970'
        },
      ],
    ],
  ].forEach((testSet) => {
    let args = testSet[0]
    let savedStats = testSet[1]
    let expectedResult = testSet[2]

    const Manager = new StorageManager(/*range*/args[0], /*score*/args[1], /*stars*/args[2], /*touches*/args[3])

    Manager.init()
    Manager.setRangeItems(5)
    Manager.setTotalItems(20)

    expect(Manager.sort(savedStats)).toEqual(expectedResult)
  })
})
