import { Animated } from 'react-native';

export class Spring {
  constructor (withStart = true) {
    this.withStart = withStart
  }

  start(prop, toValue, delay = 0, useNativeDriver = false) {
    let settings = { useNativeDriver, toValue, delay }

    return this.withStart ? Animated.spring(prop, settings).start : Animated.spring(prop, settings)
  }
}
