import { Animated, Easing } from 'react-native';

export class Timing {
  constructor (duration, withStart = true) {
    this.duration = duration
    this.withStart = withStart
  }

  start(prop, toValue, delay = 0, useNativeDriver = false) {
    let settings = { useNativeDriver, toValue, delay, easing: Easing.linear }

    this.duration && (settings.duration = this.duration)

    return this.withStart ? Animated.timing(prop, settings).start : Animated.timing(prop, settings)
  }

  static stop(prop) {
    Animated.timing(prop).stop()
  }
}
