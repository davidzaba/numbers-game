import { observable, action, computed } from 'mobx'

import { StorageManager } from '../Storage/Manager'
import { Storage } from '../Storage'

import { Time } from '../services/Time'

class GameStore {
  MAX_SELECTIONS = 2

  matches = 0
  prevSelections = [0, 0]

  @observable tip = {}
  @observable nav = {}

  @observable tween = 0
  @observable score = 0
  @observable stars = 0
  @observable selectedTop = 0
  @observable selectedDuration = 0

  @observable exit = {}

  // container to hold current numeric selections
  @observable selections = []
  @observable hasMatchingSelections = {}

  @observable replay = false
  @observable gameOver = false
  @observable isPaused = false
  @observable isJumping = false
  @observable tweenReady = false
  @observable isResolvingSelection = false

  @observable isInVerifiableState = false
  @observable successAnimationVisible = false

  /**
   * Tween-related actions
   */
  @action makeTweenReady() {
    this.tweenReady = true
  }

  /**
   *
   */
  @action resetGame() {
    this.tip = {}

    this.tween = 0
    this.matches = 0
    this.selectedTop = 0
    this.totalTouches = 0
    this.selectedDuration = 0

    this.selections = []
    this.prevSelections = [0, 0]
    this.hasMatchingSelections =  {}

    this.replay = false
    this.gameOver = false
    this.isPaused = false
    this.isJumping = false
    this.tweenReady = false
    this.isResolvingSelection = false

    this.isInVerifiableState = false
    this.successAnimationVisible = false
  }

  /**
   * Replay-related actions
   * @param val
   */
  @action setReplay(val) {
    this.replay = val
  }

  /**
   * GameOver-related actions
   */
  @action setGameOver() {
    this.resetGame()

    this.gameOver = true
  }

  @action disableGameOver() {
    this.score = 0
    this.stars = 0
    this.totalTouches = 0

    this.gameOver = false
  }

  /**
   * Choice-related actions
   * @param top
   */
  @action setChoiceTop(top) {
    this.selectedTop = top
  }

  @action setChoiceDuration(duration) {
    this.selectedDuration = duration
  }

  /**
   * Selection-related actions
   */
  @action addSelection(num) {
    if (this.isInVerifiableState) {
      return
    }

    this.selections.length !== this.MAX_SELECTIONS && this.selections.push(num)
    this.isInVerifiableState = this.selections.length === this.MAX_SELECTIONS
  }

  @action removeSelection(obj) {
    this.selections.forEach((selection, i) => {
      if (selection.num === obj.num) {
        this.selections.splice(i, 1)
      }
    })

    this.isInVerifiableState = this.selections.length === this.MAX_SELECTIONS
  }

  @action resetSelections() {
    this.selections = []
  }

  @computed get hasSelections() {
    return !!this.selections.length
  }

  @action resolveSelection(state) {
    this.isResolvingSelection = state
  }

  @action verifySelections() {
    this.isInVerifiableState = false

    let sel1 = this.selections[0].num
    let sel2 = this.selections[1].num

    let currSelections = [sel1, sel2].sort()
    this.prevSelections = this.prevSelections.sort()

    this.hasMatchingSelections = {
      time: Time.get(),
      result: JSON.stringify(currSelections) === JSON.stringify(this.prevSelections)
    }

    // identical successive selections are not allowed
    if (this.hasMatchingSelections.result) {
      return
    }

    this.prevSelections = [sel1, sel2]

    // '+' and '-'
    let addition = this.selections.reduce((a, b) => { return { num: a.num + b.num} })
    let subtraction = this.selections.reduce((a, b) => { return { num: a.num - b.num} })

    // '/' and '*'
    let division = this.selections.reduce((a, b) => { return { num: a.num / b.num} })
    let multiplication = this.selections.reduce((a, b) => { return { num: a.num * b.num} })

    // check for matches
    let isMatch = (
      addition.num === this.tween ||
      subtraction.num === this.tween ||
      division.num === this.tween ||
      multiplication.num === this.tween
    )

    // sum of good / bad choices
    this.matches = isMatch ? ++this.matches : Math.max(0, --this.matches)

    // object to notify about the latest verification
    this.tip = { isMatch, matches: this.matches, time: Time.get() }

    // get the latest score
    let score

    if (this.tip.isMatch) {
      score = this.score + this.selections.length
      this.setSuccessAnimationVisiblity(true)
      this.increaseTotalTouches()
    } else {
      score = this.score - this.selections.length
      this.setSuccessAnimationVisiblity(false)
    }

    this.score = score < 0 ? 0 : score
  }

  /**
   * PlayPause-related actions
   */
  @action playPause() {
    this.isPaused = !this.isPaused
  }

  /**
   * Jump-related actions
   * @param punish
   * @param time
   */
  @action jump(punish, time = Time.get()) {
    this.isJumping = { punish, time }
  }

  /**
   * Animation-related actions
   */
  @action setSuccessAnimationVisiblity(val) {
    this.successAnimationVisible = val
  }

  /**
   * Stars-related actions
   */
  @action setStarsCount(sum) {
    this.stars = sum
  }

  /**
   * Touches-related actions
   */
  @action increaseTotalTouches() {
    this.totalTouches++
  }

  /**
   * Global actions
   */
  @action saveGame() {
    if (!this.selectedTop || !this.score) {
      return
    }

    const Manager = new StorageManager(this.selectedTop, this.score, this.stars, this.totalTouches)

    Manager.init()
    Manager.setRangeItems(10)
    Manager.setTotalItems(40)

    Storage.retrieve().then((stats) => {
      let sortedStats = Manager.sort(stats)
      Storage.save(JSON.stringify(sortedStats))
    })
  }

  @action exitGame(time = Time.get()) {
    this.exit = { time }
  }

  @action setNav(destination,  time = Time.get()) {
    this.nav = { destination, time }
  }
}

export default new GameStore()
