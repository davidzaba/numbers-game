import { observable, action } from 'mobx'

/**
 * Layout Store
 */
class LayoutStore {
  @observable numericLayoutReady = false

  /**
   *
   */
  @action resetGame() {
    this.numericLayoutReady = false
  }

  /**
   *
   */
  @action setGameOver() {
    this.resetGame()
  }

  /**
   *
   */
  @action makeNumericLayoutReady() {
    this.numericLayoutReady = true
  }

  /**
   *
   */
  @action resetNumericLayout() {
    this.numericLayoutReady = false
  }
}

export default new LayoutStore()
