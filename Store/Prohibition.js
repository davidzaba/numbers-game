import { observable, action } from 'mobx'

/**
 * Prohibition Store
 */
class ProhibitionStore {
  @observable isInProhibitedState = false

  /**
   *
   */
  @action resetGame() {
    this.isInProhibitedState = false
  }

  /**
   *
   */
  @action setGameOver() {
    this.resetGame()
  }

  /**
   *
   */
  @action enableProhibitedState() {
    this.isInProhibitedState = true

    setTimeout(() => this.disableProhibitedState(), 200)
  }

  /**
   *
   */
  @action disableProhibitedState() {
    this.isInProhibitedState = false
  }
}

export default new ProhibitionStore()
