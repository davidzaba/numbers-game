import { observable, action, computed } from 'mobx'

import { Time } from '../services/Time'

/**
 * Choices Store
 */
class ChoicesStore {
  MAX_REFRESH_CHOICES = 3

  @observable loadedChoices = 0
  @observable refreshedChoices = 0
  @observable availableChoices = 0
  @observable refreshingChoices = false

  // max number of times to refresh numeric grid
  @observable stats = { prevSum: 0, currentSum: this.MAX_REFRESH_CHOICES }

  /**
   *
   * @return {boolean}
   */
  @computed get choicesAreReady() {
    return this.loadedChoices > 0 && this.loadedChoices === this.availableChoices
  }

  /**
   *
   * @return {boolean}
   */
  @computed get choicesAreRefreshed() {
    return this.refreshedChoices > 0 && this.refreshedChoices === this.availableChoices
  }

  /**
   *
   * @return {boolean}
   */
  @computed get refreshChoicesDepleted() {
    return !this.stats.prevSum && !this.stats.currentSum
  }

  /**
   *
   */
  @action setGameOver() {
    this.loadedChoices = 0
    this.refreshedChoices = 0
    this.availableChoices = 0

    this.stats = { prevSum: 0, currentSum: this.MAX_REFRESH_CHOICES }
  }

  /**
   *
   * @param num
   */
  @action setAvailableChoices(num) {
    this.availableChoices += Number(num)
  }

  /**
   *
   */
  @action confirmLoadedChoice() {
    this.availableChoices ? this.loadedChoices++ : (this.availableChoices = 0)
  }

  /**
   *
   */
  @action confirmRefreshedChoice() {
    this.refreshedChoices++
  }

  /**
   *
   */
  @action resetRefreshedChoices() {
    this.refreshedChoices = 0
  }

  /**
   *
   * @param time
   */
  @action upgradeRefreshChoices(time = Time.get()) {
    this.stats = {
      time,
      prevSum: Math.min(this.stats.currentSum, this.MAX_REFRESH_CHOICES),
      currentSum: Math.min(++this.stats.currentSum, this.MAX_REFRESH_CHOICES)
    }
  }

  /**
   *
   * @param time
   */
  @action downgradeRefreshChoices(time = Time.get()) {
    this.stats = {
      time,
      prevSum: Math.max(this.stats.currentSum, 0),
      currentSum: Math.max(--this.stats.currentSum, 0)
    }
  }

  /**
   *
   */
  @action toggleRefreshingChoices() {
    this.refreshingChoices = !this.refreshingChoices
  }
}

export default new ChoicesStore()
