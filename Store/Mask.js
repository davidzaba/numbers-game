import { observable, action } from 'mobx'

/**
 * Mask Store
 */
class MaskStore {
  @observable masking = false

  /**
   *
   */
  @action gameOver() {
    this.masking = false
  }

  /**
   *
   */
  @action activate() {
    this.masking = true
  }

  /**
   *
   */
  @action deactivate() {
    this.masking = false
  }
}

export default new MaskStore()
