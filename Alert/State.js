import { Alert } from '../Constants/Alert'

/**
 *
 */
export class State {

  static getInflatingColor (val) {
    // critical level
    if
    (val < Alert.Boundaries[1]) {
      return Alert.Colors.CRITICAL
    }
    // warning level
    else if
    (val < Alert.Boundaries[0]) {
      return Alert.Colors.WARNING
    }
    // normal level
    else
    {
      return Alert.Colors.NORMAL
    }
  }
}
