import React from 'react';
import { StyleSheet, Text, Button, View } from 'react-native';

export default class Header extends React.Component {
  render() {
    return (
      <View style={styles.header}>
        <Text style={styles.headerText}>{this.props.title || "MEMORY GAME"}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red'
  },

  headerText: {
    fontWeight: 'bold',
    fontSize: 25,
    textAlign: 'center'
  }
});
