import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class Score extends React.Component {
  render() {
    return (
      <View style={styles.scoreContainer}>
        <Text style={styles.score}>{this.props.score}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scoreContainer: {
    flex: 1,
    alignItems: 'center',
    padding: 10,
    backgroundColor: "yellow"
  },

  score: {
    fontWeight: 'bold',
    fontSize: 40,
  }
});
